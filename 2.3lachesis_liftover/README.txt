This folder contains a script that allows to lift over all annotations of the old Euprymna genome to the new hi-c assembly.
It also contains all necessary files: 
transcript_clusters_esc.gff - old gff annotation (Belcaid et al. 2019(
eup.softmasked.chrom.sizes - file with old scaffold sizes
Lachesis_assembly46.chrom.sizes - file with new chromosome sizes
main-results folder - this is an output folder of lachesis that has all the information of the reordered scaffolds

The output will be a new gff for the new lachesis (hi-c assembly) and a file with new scaffold locations. You can run it in the terminal with
python old_to_new_allchroms_gffformat.py
Be careful, all file paths are hardcoded, so you need to navigate to this folder to run the script. 