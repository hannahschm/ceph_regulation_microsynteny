#!/usr/bin/python
#there are 1001 N gaps between each scaffold
#gff format:
#chromosome . exon start stop . direction . geneID
import os
from collections import defaultdict

old_gff=open("transcript_clusters_esc.gff")
scaffolds=open("eup.softmasked.chrom.sizes")
scaffold_direct={}
chrom_sizes={}
lachesis_assembly=open("Lachesis_assembly46.chrom.sizes")
list_of_chroms=[]

#make a dictionary with all chromosome names and their sizes
for line in lachesis_assembly.readlines():
    columns = line.rstrip("\n").split("\t")
    chrom_name=columns[0]
    chrom_size = columns[1]
    list_of_chroms.append(chrom_name)
    if chrom_name not in chrom_sizes:
        chrom_sizes[chrom_name]=chrom_size
    else:
        continue

print(chrom_sizes)
print(list_of_chroms)


new_scaffold_locations=open("new_scaffold_locations.gff","w")
new_gff=open("esc_all_chroms_alltranscripts.gff","w")



#iterate through all files with orders of scaffolds on chromosomes and make a list for each chromosome (this is where we need os)
#make a dictionary that tells you the orientation of the scaffold on the chromosome
#create a list with only the actual chromosomes

real_chroms=[]
scaf_order_dict = defaultdict(list)
directory = "main_results"
for filename in os.listdir(directory):
    if filename.endswith(".ordering"):
        f=open("main_results/%s" % filename)
        print(filename)
        group_name=filename.rstrip(".ordering")
        group_name = next((s for s in list_of_chroms if group_name in s), None) #rename the filenames with the actual chromosome names
        print(group_name)
        real_chroms.append(group_name)
        for line in f.readlines()[11:]:
            line = line.rstrip("\n")
            columns = line.rstrip("\n").split("\t")
            scaffold_name = columns[1]
            scaf_order_dict[group_name].append(scaffold_name)
            scaffold_orientation = columns[2]
            for orientation in scaffold_orientation:
                if orientation == "1":
                    scaffold_direct[scaffold_name] = "-"
                else:
                    scaffold_direct[scaffold_name] = "+"
f.close()
print(scaf_order_dict)
print(real_chroms)


#make a dictionary with the sizes of each scaffold
scaffold_sizes={}
for line2 in scaffolds.readlines():
    columns2 = line2.rstrip("\n").split("\t")
    scaffold_size=columns2[1]
    scaffold_name=columns2[0]
    scaffold_sizes[scaffold_name]=scaffold_size
scaffolds.close()



new_start={}
new_stop={}

#make a dictionary of the scaffolds and their new start and stop coordinates
#exclude scaffolds that are not ordered on a chromosome
for group_name in real_chroms:
    print(group_name)
    new_start[group_name] = {}
    new_stop[group_name] = {}
    new_scafstart = 0
    new_scafstop = 0
    new_genestart = 0
    new_genestop = 0
    for scaffold in scaf_order_dict[group_name]:
        #print(scaffold,group_name,scaf_order_dict[group_name])
        new_scafstart = int(new_scafstop) + 1
        new_scafstop = int(new_scafstart) + int(scaffold_sizes[scaffold]) + 999
        new_start[group_name][scaffold] = new_scafstart
        new_stop[group_name][scaffold] = new_scafstop

print(new_start[group_name][scaffold])

for key, value in new_start.items():
    new_scaffold_locations.write("%s\t%s\t%s\n"%(key,value,new_stop[key]))
new_scaffold_locations.close()
#print(new_start["Lachesis_group2__55_contigs__length_182973478"])

print(new_stop)

#now add the gene coordinates as well


for line in old_gff.readlines():
    line = line.rstrip("\n").replace("=","\t").replace(",","\t")
    columns3 = line.rstrip("\n").split("\t")
    print(columns3)
    feature = columns3[2]
    gene_scaffold = columns3[0]
    # print(gene_scaffold)
    old_genestart = columns3[3]
    old_genestop = columns3[4]
    gene_direction = columns3[6]
    gene_name = columns3[8]
    transcript_name = columns3[9]
    # print(group_name)
    for group_name in new_start:
        if gene_scaffold in new_start[group_name]:
            # print(group_name)
            if scaffold_direct[gene_scaffold] == "+":
                new_genestart = int(new_start[group_name][gene_scaffold]) - 1 + int(old_genestart)
                new_genestop = int(new_start[group_name][gene_scaffold]) - 1 + int(old_genestop)
                print("%s\t%s\t%s\t%s\t.\t%s\t.\t%s\t%s\n" % (
                    group_name, new_genestart, new_genestop, width,gene_direction, gene_name,feature))
                new_gff.write("%s\t.\texon\t%s\t%s\t.\t%s\t.\t%s;%s;%s;%s\n" % (
                    group_name, new_genestart, new_genestop, gene_direction, gene_name, gene_scaffold,scaffold_direct[gene_scaffold],transcript_name))
            else:
                new_genestop = int(new_stop[group_name][gene_scaffold]) - 1000 - int(old_genestart)
                new_genestart = int(new_stop[group_name][gene_scaffold]) - 1000 - int(old_genestop)
                width=int(new_genestop)-int(new_genestart)
                print("%s\t%s\t%s\t%s\t.\t%s\t.\t%s\t%s\n" % (
                    group_name, new_genestart, new_genestop, width,gene_direction, gene_name,feature))
                new_gff.write("%s\t.\texon\t%s\t%s\t.\t%s\t.\t%s;%s;%s;%s\n" % (
                    group_name, new_genestart, new_genestop, gene_direction, gene_name,gene_scaffold,scaffold_direct[gene_scaffold],transcript_name))

new_gff.close()


gene_dict_start={}