This folder contains three scripts; 
1. find_microsynteny_groups.py - this script allows you to extract different groups from the original clustering file.
The clustering file contains all microsyntenic blocks between all species. The script lets you e.g. extract ceph.clus 
(cephalopod specific syntenies) or meta.clus (conserved, metazoan syntenies). If you want to have a look at the available
categories just open the script in a txt reader.
To run the script you need the file 5_allspecies.clus which includes all syntenic blocks (list of all syntenic blocks per line with gene content). It was included in the folder.
If you want, you can compare your results to ceph.clus or meta.clus in the folder Tree_method. The script will automatically 
create these files if you run it with python find_microsynteny_groups.py . You can also have a look at the script and change the categories.
2. countmicrosyntenies_with_blocks.py - this script was used to count blocks within each category. The files were first created in step 1
and then used to count all blocks present in a specific lineage. All files usedfor this script are in the subfolder blocks. You can 
run it with python countmicrosyntenies_with_blocks.py and it will count all clade specific blocks for you. This file relies on the clustering file
and all its connections 5.blocks.3.syn.cluster (orthologous relationships). This is important because it allows to exclude orthologous blocks, which are included in the clustering file.
3. rpz_pick_random_blocks.R - (written by Bob Zimmermann, slightly modified from Zimmermann, Technau & Simakov 2019, see https://github.com/nijibabulu)
This script allows to compute random blocks as used in the paper. If you want to for example compute 20 random iterations of cephalopod-specific synteny
run: Rscript rpz_pick_random_blocks.R --only-genome=EUPSC -n 20 ceph_random ceph.clus EUPSC.chrom, where ceph_random is the output name, ceph.clus is the clustering
file after which you want to model your random sampling and EUPSC.chrom is an annotation file with genomic locations (included in this folder)