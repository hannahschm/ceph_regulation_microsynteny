#!/usr/bin/python
####counting the mollusk syntenies###
print("counting mollusks <3")
import re
species_dict={}


snails=["LOTGI","APLCA"]
mussels=["CRAGI","MIZYE"]
cephs=["EUPSC","CALMI","OCTBI"]
squid=["EUPSC"]
octopusses=["CALMI","OCTBI"]


####counting the mollusk syntenies###
print("counting mollusks <3")

species_dict={}
with open("blocks/mollusc.clus") as synteny:
    for line in synteny.readlines():
        columns = line.rstrip("\n").split("\t")
        #print(columns)
        #print(columns[5].count(","))
        if columns[0] not in species_dict:
            species_dict[columns[0]]=columns[1]
#print(species_dict)

snails=["LOTGI","APLCA"]
mussels=["CRAGI","MIZYE"]
cephs=["EUPSC","CALMI","OCTBI"]
mollusc_count=0
snail_count=0
mussel_count=0
snail_mussel_count=0
with open("blocks/5.blocks.3.syn.clusters") as blocks:
    for line in blocks.readlines():
        line=line.rstrip("\n")
        #columns = line.rstrip("\n").split("\t")
        #print(line)
        synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
        #print(line)
        #print(synt_list)
        species=[]
        for syntid in synt_list: #check id in the line
            if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
                species.append(species_dict[syntid])
        #print(species)
        if len(species)>0:
            sn_dict = {}
            mu_dict = {}
            ce_dict = {}
            #print(species)
            for specimen in species:
                # print(specimen)
                if specimen in snails:
                    sn_dict[specimen] = 1
                    #print ("%s is in %s"%(specimen, snails))
                elif specimen in mussels:
                    mu_dict[specimen] = 1
                elif specimen in cephs:
                    ce_dict[specimen] = 1
            #print(species)
            #print(len(sn_dict.keys()))
            #print(len(mu_dict.keys()))
            #print(len(ce_dict.keys()))
            if len(sn_dict.keys())>0 and len(mu_dict.keys())==0 and len(ce_dict.keys())==0:
                snail_count = snail_count+1
            if len(ce_dict.keys())==0 and len(mu_dict.keys())>0 and len(sn_dict.keys())==0:
                mussel_count = mussel_count+1
            if len(ce_dict.keys())>0 and len(mu_dict.keys())>0 and len(sn_dict.keys())>0:
                mollusc_count = mollusc_count+1
            if len(ce_dict.keys())==0 and len(mu_dict.keys())>0 and len(sn_dict.keys())>0:
                snail_mussel_count=snail_mussel_count+1

squid=["EUPSC"]
octos=["CALMI","OCTBI"]
oc_count=0
ceph_count=0
test_count=0
count_oct=0
species_dict={}
old_ceph_list_lists=[]
new_ceph_list=[]
old_ceph_list = []
EUPSC_list=[]
with open("blocks/ceph.clus") as synteny:
    for line in synteny.readlines():
        columns = line.rstrip("\n").split("\t")
        #print(columns)
        #print(columns[5].count(","))
        if columns[0] not in species_dict:
            species_dict[columns[0]]=columns[1]
        if "EUPSC" in columns[1]:
            test_count=test_count+1
            #old_ceph_list_lists.append(old_ceph_list)
        if columns[1] == "OCTBI":
            # print(line)
            if "EUPSC" not in columns[5]:
                # print(line)
                count_oct = count_oct + 1
print(species_dict)

test_eups=[]
test_eups2=[]
with open("blocks/5.blocks.3.syn.clusters") as blocks:
    for line in blocks.readlines():
        line=line.rstrip("\n")
        #columns = line.rstrip("\n").split("\t")
        #print(line)
        synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
        #print(line)
        #print(synt_list)
        species=[]
        eupsc_count=0
        for syntid in synt_list: #check id in the line
            if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
                species.append(species_dict[syntid])
        #print(species)
        if len(species)>0:
            sq_dict = {}
            oc_dict = {}
            #print(species)
            for specimen in species:
                #print(specimen)
                if specimen in squid:
                    sq_dict[specimen] = 1
                    #print ("%s is in %s"%(specimen, snails))
                elif specimen in octos:
                    oc_dict[specimen] = 1
            #print(species)
            #print(len(sn_dict.keys()))
            #print(len(mu_dict.keys()))
            #print(len(ce_dict.keys()))
            if len(sq_dict.keys())>0 and len(oc_dict.keys())>0 :
                #print(sq_dict.keys(),len(sq_dict.keys()),oc_dict.keys(),len(oc_dict.keys()))
                for synt_id in synt_list:
                    new_ceph_list.append(int(synt_id))
                ceph_count = ceph_count+ 1
            if len(oc_dict.keys())>0 and len(sq_dict.keys())==0:
                oc_count=oc_count+1


molluscs=["EUPSC","CALMI","OCTBI","CRAGI","MIZYE","LOTGI","APLCA"]
annelids=["CAPTE","HELRO"]
platy=["SCHMA","ADIVA"]
lopho_count=0
species_dict={}
with open("blocks/lopho.clus") as synteny:
    for line in synteny.readlines():
        columns = line.rstrip("\n").split("\t")
        #print(columns)
        #print(columns[5].count(","))
        if columns[0] not in species_dict:
            species_dict[columns[0]]=columns[1]
#print(species_dict)

with open("blocks/5.blocks.3.syn.clusters") as blocks:
    for line in blocks.readlines():
        line=line.rstrip("\n")
        #columns = line.rstrip("\n").split("\t")
        #print(line)
        synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
        #print(line)
        #print(synt_list)
        species=[]
        for syntid in synt_list: #check id in the line
            if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
                species.append(species_dict[syntid])
        #print(species)
        if len(species)>0:
            mol_dict = {}
            an_dict = {}
            pla_dict = {}
            #print(species)
            for specimen in species:
                #print(specimen)
                if specimen in molluscs:
                    mol_dict[specimen] = 1
                    print("adding mollusk %s" % specimen)
                    #print ("%s is in %s"%(specimen, snails))
                elif specimen in annelids:
                    an_dict[specimen] = 1
                    print("adding annelid %s" % specimen)
                elif specimen in platy:
                    pla_dict[specimen] = 1
                    print("adding pla %s" % specimen)
            #print(species)
            #print(len(sn_dict.keys()))
            #print(len(mu_dict.keys()))
            #print(len(ce_dict.keys()))
            if len(mol_dict.keys())>0 and len(an_dict.keys())>0 and len(pla_dict.keys())>0:
                print(mol_dict.keys(),an_dict.keys(),pla_dict.keys())
                lopho_count = lopho_count+ 1
            elif len(mol_dict.keys())>0 and len(an_dict.keys())>0 and len(pla_dict.keys())==0:
                print(mol_dict.keys(),an_dict.keys(),pla_dict.keys())
                lopho_count = lopho_count+ 1




nematodes=["CAEEL"]
insects=["STEMI","TRICA","DROME"]
ecdy_count=0
insect_count=0
species_dict={}
with open("blocks/ecdy.clus") as synteny:
    for line in synteny.readlines():
        columns = line.rstrip("\n").split("\t")
        #print(columns)
        #print(columns[5].count(","))
        if columns[0] not in species_dict:
            species_dict[columns[0]]=columns[1]
#print(species_dict)

with open("blocks/5.blocks.3.syn.clusters") as blocks:
    for line in blocks.readlines():
        line=line.rstrip("\n")
        #columns = line.rstrip("\n").split("\t")
        #print(line)
        synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
        #print(line)
        #print(synt_list)
        species=[]
        for syntid in synt_list: #check id in the line
            if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
                species.append(species_dict[syntid])
        #print(species)
        if len(species)>0:
            nem_dict = {}
            in_dict = {}
            #print(species)
            for specimen in species:
                #print(specimen)
                if specimen in insects:
                    in_dict[specimen] = 1
                    print("adding insect %s" % specimen)
                    #print ("%s is in %s"%(specimen, snails))
                elif specimen in nematodes:
                    nem_dict[specimen] = 1
                    print("adding nematode %s" % specimen)
            #print(species)
            #print(len(sn_dict.keys()))
            #print(len(mu_dict.keys()))
            #print(len(ce_dict.keys()))
            if len(nem_dict.keys())>0 and len(in_dict.keys())>0:
                print(mol_dict.keys(),an_dict.keys(),pla_dict.keys())
                ecdy_count = ecdy_count+ 1
            if len(nem_dict.keys())==0 and len(in_dict.keys())>0:
                insect_count = insect_count + 1


chordata=["BRAFL","CIOIN","MOUSE","HUMAN"]
ambulacraria=["STRPU","SACKO"]
vertebrata=["MOUSE","HUMAN"]
amb_count=0
deutero_count=0
chord_count=0
species_dict={}
with open("blocks/deutero.clus") as synteny:
    for line in synteny.readlines():
        columns = line.rstrip("\n").split("\t")
        #print(columns)
        #print(columns[5].count(","))
        if columns[0] not in species_dict:
            species_dict[columns[0]]=columns[1]
#print(species_dict)

with open("blocks/5.blocks.3.syn.clusters") as blocks:
    for line in blocks.readlines():
        line=line.rstrip("\n")
        #columns = line.rstrip("\n").split("\t")
        #print(line)
        synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
        #print(line)
        #print(synt_list)
        species=[]
        for syntid in synt_list: #check id in the line
            if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
                species.append(species_dict[syntid])
        #print(species)
        if len(species)>0:
            chord_dict = {}
            amb_dict = {}
            #print(species)
            for specimen in species:
                #print(specimen)
                if specimen in chordata:
                    chord_dict[specimen] = 1
                    print("adding chordata %s" % specimen)
                    #print ("%s is in %s"%(specimen, snails))
                elif specimen in ambulacraria:
                    amb_dict[specimen] = 1
                    print("adding ambulacraria %s" % specimen)
            #print(species)
            #print(len(sn_dict.keys()))
            #print(len(mu_dict.keys()))
            #print(len(ce_dict.keys()))
            if len(amb_dict.keys())>0 and len(chord_dict.keys())>0:
                deutero_count=deutero_count+1
            if len(amb_dict.keys())>0 and len(chord_dict.keys())==0:
                amb_count=amb_count+1
            if len(amb_dict.keys())==0 and len(chord_dict.keys())>0:
                chord_count=chord_count+1



synt_id_list=[]
count_meta_test=0
with open("blocks/meta.clus") as syntenies:
    for line in syntenies.readlines():
        columns=line.rstrip("\n").split("\t")
        #print(columns)
        if columns[1] == "EUPSC":
            count_meta_test = count_meta_test + 1

# cnidaria=["NEMVE"]
# sponge=["AMPQU"]
# ctenophora=["MNELE"]
# synt_id_list=[]
# meta_count=0
# with open("../meta.clus") as synteny:
#     for line in synteny.readlines():
#         columns = line.rstrip("\n").split("\t")
#         #print(columns)
#         #print(columns[5].count(","))
#         if columns[0] not in species_dict:
#             species_dict[columns[0]]=columns[1]
# #print(species_dict)
#
# with open("../5.blocks.3.syn.clusters") as blocks:
#     for line in blocks.readlines():
#         line=line.rstrip("\n")
#         #columns = line.rstrip("\n").split("\t")
#         #print(line)
#         synt_list=line.rstrip("\n").split("\t")[1:] #need to skip the first item as thats just the row numbering
#         #print(line)
#         #print(synt_list)
#         species=[]
#         for syntid in synt_list: #check id in the line
#             if syntid in species_dict: #if the id is in the species dict, which is eg. id=species, add it to a list
#                 species.append(species_dict[syntid])
#         #print(species)
#         if len(species)>0:
#             chord_dict = {}
#             amb_dict = {}
#             mol_dict = {}
#             an_dict = {}
#             pla_dict = {}
#             ins_dict = {}
#             ne_dict = {}
#             cn_dict = {}
#             sp_dict = {}
#             ct_dict = {}
#             #print(species)
#             for specimen in species:
#                 #print(specimen)
#                 if specimen in cordata:
#                     chord_dict[specimen] = 1
#                     print("adding chordata %s" % specimen)
#                     #print ("%s is in %s"%(specimen, snails))
#                 elif specimen in ambulacraria:
#                     amb_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in molluscs:
#                     mol_dict[specimen] = 1
#                     print("adding mollusk %s" % specimen)
#                 elif specimen in annelids:
#                     an_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in platy:
#                     pla_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in insects:
#                     ins_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in nematodes:
#                     ne_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in cnidaria:
#                     cn_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in ctenophora:
#                     ct_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 elif specimen in sponge:
#                     sp_dict[specimen] = 1
#                     print("adding ambulacraria %s" % specimen)
#                 if sp_dict>0 or ct_dict>0 or cn_dict>0 and
#
#             #print(species)
#             #print(len(sn_dict.keys()))
#             #print(len(mu_dict.keys()))
#             #print(len(ce_dict.keys()))
#             if len(amb_dict.keys())>0 and len(chord_dict.keys())>0:
#                 deutero_count=deutero_count+1
#             if len(amb_dict.keys())>0 and len(chord_dict.keys())==0:
#                 amb_count=amb_count+1
#             if len(amb_dict.keys())==0 and len(chord_dict.keys())>0:
#                 chord_count=chord_count+1



print("There are %s mollusk syntenies"% mollusc_count)
print("There are %s snail syntenies"% snail_count)
print("There are %s mussel syntenies"% mussel_count)
print("There are %s snail/mussel syntenies"% snail_mussel_count)
print("There are %s ceph syntenies"% ceph_count)
print("There are %s octopus syntenies"% oc_count)
print("There are %s lophotrochozoa syntenies" % lopho_count)
print("There are %s insect syntenies"% insect_count)
print("There are %s ecdysozoans syntenies" % ecdy_count)
print("There are %s deuterostome syntenies" % deutero_count)
print("There are %s ambulacraria syntenies" % amb_count)
print("There are %s chordate syntenies" % chord_count)
