#!/usr/bin/python

ceph_list=["EUPSC", "OCTBI", "CALMI"]
mollusc_list=["EUPSC", "OCTBI", "CALMI","LOTGI","CRAGI","APLCA","MIZYE"]
lopho_list=["EUPSC", "OCTBI", "CALMI","HELRO","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","SCHMA"]
meta_list=["SACKO","EUPSC", "OCTBI", "CALMI","HELRO","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","BRAFL","HUMAN","CAEEL","DROME","CIOIN","STRPU","STEMI","TRICA","ADIVA","NEMVE","AMPQU","MNELE","SCHMA","MOUSE"]
bilat_list=["EUPSC", "OCTBI", "CALMI","HELRO","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","BRAFL","HUMAN","CAEEL","DROME","CIOIN","STRPU","STEMI","TRICA","ADIVA","SCHMA","MOUSE"]
vertebrate_list=["HUMAN","MOUSE"]
deuterostome_list=["BRAFL","CIOIN","MOUSE","HUMAN","STRPU","SACKO"]
protostome_list=["EUPSC", "OCTBI", "CALMI","HELRO","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","CAEEL","DROME","STEMI","TRICA","ADIVA","SCHMA"]
insect_list=["DROME","STEMI","TRICA"]
list_of_lists=[ceph_list,mollusc_list,lopho_list,meta_list,bilat_list,vertebrate_list,deuterostome_list,protostome_list,insect_list]
snail_mussel_list=["LOTGI","CRAGI","APLCA","MIZYE"]
ecdysozoa_list=["STEMI","TRICA","DROME","CAEEL"]
mussels_list=["CRAGI","MIZYE"]

#print(list_of_lists)

def find_syntgroups (synt_file, synt_type):
    with open(synt_file) as synteny:
        group = 0
        synteny_lines=[]
        for line in synteny.readlines():
            columns = line.rstrip("\n").split("\t")
            if all(species in synt_type for species in columns[5].split(",")):
                #print("is in %s %s" % (synt_type, columns[5]))
                synteny_lines.append(line)
                group=group+1
            else:
                continue
    return synteny_lines
    #print("there are %s synteny clusters in this group"%(group))



def find_syntgroups_min (synt_file, synt_type, min_overlap):
    with open(synt_file) as synteny:
        group = 0
        synteny_lines = []
        for line in synteny.readlines():
            columns = line.rstrip("\n").split("\t")
            if all(len(columns[5].split(","))>=min_overlap and species in synt_type for species in columns[5].split(",")):
                #print("is in %s %s" % (synt_type, columns[5]))
                #print(line)
                synteny_lines.append(line)
                group=group+1
    return synteny_lines
    #print("there are %s synteny clusters in this group"%(group))

print("This script will automatically create the clusering files ceph.clus and meta.clus as they were used in the paper")
print("You can also open the script and play with the parameters if you want other results")
print("The script will save a file ceph.clus, whcih filters all syntenic blocks for blocks present in only cephalopod and in at least two ceph species")
print("The script will save a file meta.clus, whcih filters all syntenic blocks for blocks present in metazoans and in at least seven species")
print("You will find these files in the Synteny_clusering folder and can compare them with the example files in the Tree_method folder")
with open("ceph.clus", "w") as f:
    for item in (find_syntgroups_min("5_allspecies.clus", ceph_list, 2)):
        f.write("%s" % item)
with open("meta.clus", "w") as f:
    for item in (find_syntgroups_min("5_allspecies.clus", meta_list, 7)):
                f.write("%s" % item)

