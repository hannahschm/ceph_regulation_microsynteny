This script was used to prepare the circos plot. We need both clustering files (5.blocks.3.syn.clusters (orthologous relationships) and 5_allspecies.clus (list of all syntenic blocks per line with gene content)).
It gives an output for all syntenic clusters between each species pair, for mollusks, cephalopod and "metazoan" syntenies.
This can then be visualized in R with the circlize package.
You can then run R studio to run the R script and visualize the results.