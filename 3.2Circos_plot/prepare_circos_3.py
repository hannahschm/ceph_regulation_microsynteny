import re
from collections import defaultdict
count = {}
synt_blocks = open("../Synteny_clustering/blocks/5.blocks.3.syn.clusters") #Output from synteny analysis, example files in the Synteny_clustering folder. This file contains information about all connections, including orthologous clusters
synteny = open("../Synteny_clustering/5_allspecies.clus") #this file contains information about all clusters, but listing non orthologues clusters. Thats why we need both files,
# synteny_meta=open("../hic_phylogeny/meta.clus")
synt_id_list = {}

ceph_list=["EUPSC", "OCTBI", "CALMI"]
mollusk_list=["EUPSC", "OCTBI", "CALMI","LOTGI","CRAGI","APLCA","MIZYE"]
meta_list=["SACKO","EUPSC", "OCTBI", "CALMI","HELRO","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","BRAFL","HUMAN","CAEEL","DROME","CIOIN","STRPU","STEMI","TRICA","ADIVA","NEMVE","AMPQU","MNELE","SCHMA","MOUSE"]


count=0
cluster_dict=defaultdict(list)

id_dict={}
for line in synteny.readlines():
    columns = line.rstrip("\n").split("\t")
    id_dict[columns[0]]=columns[1]
#print(id_dict)

result=open("temp_result.txt","w")
for line in synt_blocks.readlines():
    line = line.rstrip("\n").split("\t", 1)[1]
    columns = line.rstrip("\n").split("\t")
    # print(columns)
    # print(items)
    #remove the first word
    for item in line.split("\t"):
        #print(line)
        count=count+1
        #print(item)
        print(("%s," % (id_dict[item])))
        result.write("%s," % (id_dict[item]))
    if count in cluster_dict:
        cluster_dict[count].append(line)
    else:
        cluster_dict[count]=[line]
    print("\t%s\n" %count)
    result.write("\t%s\n" %count)
    #print("there are %s ids in this line" % count)
    count = 0
result.close()
#print(cluster_dict)
result=open("temp_result.txt")


result.close()
combi_count={}
combi_dict=defaultdict(list)
combi_list=[]
unique_id=0
result=open("temp_result.txt")
species=[]
synt_type={}
count_dict={}
type_dict={}
for line in result.readlines():
    columns = line.rstrip("\n").split("\t")
    print("this is result")
    block_list=((columns[0][:-1]).split(","))
    #print(species)
    #remobe double values
    unique_species_block=set(block_list)
    unique_species_list = list(unique_species_block)
    print(block_list)
    #unique_species_block = list(unique_species_block)
    print(unique_species_block)
    #seperator = ', '
    #block = seperator.join(block_list)
    #print(block)
    #kick out blocks of only one species
    if int(columns[1]) == int(1):
        continue
    #kick out large blocks with more than 30 species
    if int(columns[1]) >= int(30):
        continue
    if unique_species_block not in combi_list:
        unique_id=unique_id+1
        count_dict[unique_id]=unique_species_block
        combi_list.append(unique_species_block)
        combi_dict[unique_id].append(block_list)
        if int(columns[1]) >= int(7) :
            type_dict[unique_id] = "meta"
        elif all(int(columns[1]) >= 5 and species in mollusk_list for species in unique_species_list):
            print("THESE ARE MOLLUSKS")
            print(species)
            print(mollusk_list)
            type_dict[unique_id] = "mollusk"
        elif all(int(columns[1]) >= 2 and species in ceph_list for species in unique_species_list):
            type_dict[unique_id] = "ceph"
        else:
            if unique_id not in type_dict:
                type_dict[unique_id]="others"
                print(unique_species_list)
    else:
        #get the blocks and compare them to the new block using set so that doubles are not counted and the combination isnt relevant
        for unique_id, block in count_dict.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
            if block == unique_species_block:
                combi_dict[unique_id].append(block_list)
                print("Yeaaaaaaahhhhhhhhh")
                print(block,unique_species_block)
            else:
                #print("WWWWWWWHHHHHHHAAAAAAAAAAAAAAA")
                #print(block, unique_species_block)
                continue
print(combi_dict)
print(type_dict)
print(count_dict)
#we also need a dictionary that keeps track of the start and stops in the species themselves! for that we use the first species in a pair

#os.remove("temp_result.txt")
result2=open("circos_interactions1.txt","w")
result3=open("circos_interactions2.txt","w")
#make a list with the species you want to have in the plot and ignore the ones that are not in the list
#myspecies=["SACKO","EUPSC", "OCTBI", "CALMI","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","BRAFL","CAEEL","DROME","NEMVE","AMPQU","MNELE","MOUSE"]
#take out mouse
myspecies=["SACKO","EUPSC", "OCTBI", "CALMI","LOTGI","CRAGI","APLCA","CAPTE","MIZYE","BRAFL","CAEEL","DROME","NEMVE","AMPQU","MNELE"]

color={}
positions={}
#sort combi_dict by length of values and safe them in a list
keylist=[]
keylist_ceph=[]
keylist_mollusk=[]
keylist_meta=[]
for k in sorted(combi_dict, key=lambda k: len(combi_dict[k]), reverse=True):
    if type_dict[k]=="ceph":
        keylist_ceph.append(k)
        continue
    elif type_dict[k]=="mollusk":
        keylist_mollusk.append(k)
        continue
    elif type_dict[k]=="meta":
        keylist_meta.append(k)
        continue
    else:
        keylist.append(k)
        print (k)
keylist.extend(keylist_meta)
keylist.extend(keylist_mollusk)
keylist.extend(keylist_ceph)



for key in keylist:
    # res = list(zip(values, values[1:] + values[:1]))
    print(key)
    print(combi_dict[key])
    print(len(combi_dict[key]))
    species = set(combi_dict[key][0])
    species = list(species)
    res = list(zip(species, species[1:] + species[:1]))
    # res = res[:-1]
    print(res)
    starta = 0
    stopa = 0
    startb = 0
    stopb = 0
    width = len(combi_dict[key])
    for a, b in res:
        if a not in myspecies:
            print(a)
            continue
        elif a not in positions:
            positions[a] = 0
        if b not in myspecies:
            print(a)
            continue
        elif b not in positions:
            positions[b] = 0

        starta = positions[a] + 1
        stopa = positions[a] + width

        startb = positions[b] + 1
        stopb = positions[b] + width

        if type_dict[key] == "ceph":
            if len(res)==2:
                color = "#56B4E9"
            else:
                color = "#0072B2"
        elif type_dict[key] == "meta":
            color = "#E69F00"
        elif type_dict[key] == "mollusk":
            color = "#009E73"
        elif type_dict[key] == "others":
            color = "#666666"
        print(a, b, starta, stopa, key, type_dict[key])
        result2.write("%s\t%s\t%s\t%s\t%s\n" % (a, starta, stopa, color, type_dict[key]))
        result3.write("%s\t%s\t%s\t%s\t%s\n" % (b, startb, stopb, color, type_dict[key]))

    for a in species:
        if a not in myspecies:
            print(a)
            continue
        else:
            positions[a] = positions[a] + width

result2.close()
result3.close()
