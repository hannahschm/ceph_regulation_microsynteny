This folder contains scripts to calculate gene distances, 
get whole gene locations (starting from the first exon , ending after the last exon) 
and to filter the annotation to retain only the longest transcripts. 
These scripts were specifically used for Euprymna, because our gff does not contain gene models, only exons.
The folder includes a gff file that was lifted over from the published Euprymna assembly to use as annotation 
for the new hi-c assembly (see also lachesis_liftover).

1. get_first_last_exon.py lets you filter the gff for whole gene locations. It will save a file called esc_whole_genes.gff in the same folder.
2. Only_longest_transcript.py filters the gff with whole genes to only include the longest annotated transcripts. When your un it it gives you a new gff called "esc_whole_genes_unique.gff"
This was then used to plot the karyotypes of figure 
3.gene_distances_synt_type.py - this script calculates intergenic distances using the output of script number 2 and the clustering files

R script - Please make sure you have all the necessary libraries installed. They are listed at the beginning of the script. We also copied a version of the scallop gff for plotting to this folder.
For plotting euprymna exons we used this file esc_allchroms_uniq_15jul2019.nonred.gtf which is also present in the folder. It is the same as esc_all_chroms_alltranscripts.gff, filtered to only include the longest transcripts but retain the exon structure of the genes. 

All additional files in this folder were created with these script and can be overwritten :)