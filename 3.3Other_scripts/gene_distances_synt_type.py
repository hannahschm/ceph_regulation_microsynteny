#!/usr/bin/python
#we want to see if the distances between genes are different between ceph specific synteny and metazoan synteny
#using itertools we can iterate through pairs in a list
#what we need is the gff with whole genes (contains gene start and stop, not exons),
#we need the synteny file to make a list of the genes


gff=open("esc_whole_genes_unique.gff")
ceph_synteny=open("../Tree_method/ceph.clus")
meta_synteny=open("../Tree_method/meta.clus")
gene_start={}
gene_stop={}
for line in gff.readlines():
    line=line.rstrip("\n")
    columns=line.split("\t")
    #print(line)
    gene="EUPSC_"+columns[3]
    if gene not in gene_start:
        gene_start[gene]=columns[1]
        gene_stop[gene]=columns[1]
    else:
        #this is just a safety test
        print("%s is already in the dictionary even though it should not be!!!!" %(gene))
gff.close()

from collections import defaultdict
synt_list=defaultdict(list)
for line in ceph_synteny.readlines():
    line = line.rstrip("\n")
    columns = line.split("\t")
    print(line)
    if columns[1]=="EUPSC":
        synt_list[columns[0]]=columns[9].split(",")
print(synt_list)
ceph_synteny.close()

distances=open("intergenic_distances_ceph.synt","w")
#loop over the syntenic clusters
for synteny in synt_list:
    #get pairs of genes
    for x in zip(synt_list[synteny][:-1],synt_list[synteny][1:]):
        #now we can access the two genes by list slicing and get the stop of the first gene +1 and the start of the second gene -1
        print(synteny,int(gene_stop[x[0]]),int(gene_start[x[1]]),(int(gene_start[x[1]]))-(int(gene_stop[x[0]])),x[0],x[1])
        distances.write("%s\t%s\t%s\t%s\t%s\t%s\n" %(synteny,int(gene_stop[x[0]]),int(gene_start[x[1]]),(int(gene_start[x[1]]))-(int(gene_stop[x[0]])),x[0],x[1]))
        #print(x[1])
distances.close()

from collections import defaultdict
synt_list=defaultdict(list)
for line in meta_synteny.readlines():
    line = line.rstrip("\n")
    columns = line.split("\t")
    print(line)
    if columns[1]=="EUPSC":
        synt_list[columns[0]]=columns[9].split(",")
print(synt_list)
ceph_synteny.close()

distances=open("intergenic_distances_meta.synt","w")
#loop over the syntenic clusters
for synteny in synt_list:
    #get pairs of genes
    for x in zip(synt_list[synteny][:-1],synt_list[synteny][1:]):
        #now we can access the two genes by list slicing and get the stop of the first gene +1 and the start of the second gene -1
        print(synteny,int(gene_stop[x[0]]),int(gene_start[x[1]]),(int(gene_start[x[1]]))-(int(gene_stop[x[0]])),x[0],x[1])
        distances.write("%s\t%s\t%s\t%s\t%s\t%s\n" %(synteny,int(gene_stop[x[0]]),int(gene_start[x[1]]),(int(gene_start[x[1]]))-(int(gene_stop[x[0]])),x[0],x[1]))
        #print(x[1])
distances.close()
