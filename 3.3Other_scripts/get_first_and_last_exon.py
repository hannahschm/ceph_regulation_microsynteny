#!/usr/bin/python
#This script filters the locations of exons and gives you a file with whole gene locations
#We included a gff file for Euprymna in this folder. The gff is based on the published Euprymna genome, but locations
#were lifted over to match the new hi-c assembly
gff=open("esc_all_chroms_alltranscripts.gff")
chrom_list=[]
import re
gene_start_dict={}
gene_stop_dict={}
chromosome_dict={}
gene_list=[]
transcript_list=[]
transcript_dict={}
for line in gff.readlines():
    columns = line.rstrip("\n").split("\t")
    names = re.split(r';', columns[8])
    print(columns)
    print(names)
    if int(columns[3])<= int(columns[4]):
        #print(columns[3],columns[4])
        if names[3] not in gene_start_dict:
            gene_start_dict[names[3]]=columns[3]
            transcript_dict[names[3]]=names[0]
            #print(gene_start_dict[names[0]])
            chromosome = re.findall(r'^Lachesis_group(\d+)', columns[0])
            chromosome_dict[names[3]]=chromosome[0]
            gene_list.append(names[3])
            #print(chromosome_dict[names[0]])
        elif int(columns[3])<int(gene_start_dict[names[3]]):
            #print("gene is in dict but new start is smaller")
            #print(gene_start_dict[names[3]])
            #print(columns[3])
            gene_start_dict[names[3]] = columns[3]
        else:
            continue
        if names[3] not in gene_stop_dict:
            gene_stop_dict[names[3]]=columns[4]
        elif int(columns[4])>int(gene_stop_dict[names[3]]):
            gene_stop_dict[names[3]] = columns[4]
        else:
            continue
    else:
        print("omgggggggggggggggggggggggggggggggggggggggggggggggggggg")
        if names[3] not in gene_start_dict:
            gene_start_dict[names[3]] = columns[4]
            transcript_dict[names[3]] = names[0]
            # print(gene_start_dict[names[0]])
            chromosome = re.findall(r'^Lachesis_group(\d+)', columns[0])
            chromosome_dict[names[3]] = chromosome[0]
            gene_list.append(names[3])
            # print(chromosome_dict[names[0]])
        elif int(columns[4]) < int(gene_start_dict[names[3]]):
            # print("gene is in dict but new start is smaller")
            # print(gene_start_dict[names[0]])
            # print(columns[3])
            gene_start_dict[names[3]] = columns[4]
        else:
            continue
        if names[3] not in gene_stop_dict:
            gene_stop_dict[names[3]] = columns[3]
        elif int(columns[3]) > int(gene_stop_dict[names[3]]):
            gene_stop_dict[names[3]] = columns[3]
        else:
            continue



#print(chromosome_dict)
#print(chromosome_dict)
gff.close()
whole_genes=open("esc_whole_genes.gff","w")
for gene in gene_list:
    #print(gene)
    #print(chromosome_dict[gene])
    #print(gene_start_dict[gene])
    #print(gene_stop_dict[gene])
    #print(transcript_dict[gene])
    length=int(gene_stop_dict[gene])-int(gene_start_dict[gene])
    if length<0:
        print("length is smaller than zero")
        print(gene,transcript_dict[gene])
    whole_genes.write("chr%s\t%s\t%s\t%s\t%s\t%s\n" % (chromosome_dict[gene],gene_start_dict[gene],gene_stop_dict[gene],transcript_dict[gene],gene,length))

whole_genes.close()