#!/usr/bin/python
gff=open("esc_whole_genes.gff")
length_dict={}
start_dict={}
stop_dict={}
chrom_dict={}
gene_list=[]
for line in gff.readlines():
    columns = line.rstrip("\n").split("\t")
    print(columns)
    length=columns[5]
    if columns[3] not in length_dict:
        length_dict[columns[3]]=columns[5]
        gene_list.append(columns[3])
        start_dict [columns[3]]= [columns[1]]
        stop_dict[columns[3]] = [columns[2]]
        chrom_dict[columns[3]] = [columns[0]]

    elif columns[5]>length_dict[columns[3]]:
        length_dict[columns[3]] = columns[5]
        start_dict [columns[3]]= [columns[1]]
        stop_dict[columns[3]] = [columns[2]]
        chrom_dict[columns[3]] = [columns[0]]
    else:
        continue

unique_whole_genes=open("esc_whole_genes_unique.gff","w")
for gene in gene_list:
    print(gene)
    print(chrom_dict[gene])
    print(start_dict[gene])
    print(stop_dict[gene])
    unique_whole_genes.write("%s\t%s\t%s\t%s\n" % (chrom_dict[gene][0],start_dict[gene][0],stop_dict[gene][0],gene))

unique_whole_genes.close()
