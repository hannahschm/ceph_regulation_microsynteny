For figure 2b tads were predicted for human the same way as for E. scolopes (see also supplementary methods for data source et.). The figure was done using the R script provided here. 

Files: all_tads_esc_new.tad these are Tads predicted by Tadbit for E. scolopes (100000bp window size)

Files: all_tads_human.tad these are Tads predicted by Tadbit for human (100000bp window size)