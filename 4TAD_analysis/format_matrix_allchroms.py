#!/usr/bin/python

import numpy as np
import pandas as pd
import os
#import seaborn


print("Please give me the direction to the matrix files you created with the scripts in the folder Tree method")
file_path=input()
print("Thank you, I will recompute your matrices. They will be saved as .csv files in the same folder as your original matrices")
directory=os.fsencode(file_path)
for file in os.listdir(directory):
    filename=os.fsdecode(file)
    if filename.startswith("Lachesis_group") and filename.endswith(".matrix"):
        matrix=open("%s/%s" % (file_path, filename))
        chrom=filename.replace(".matrix","")
        #convert simple long format to mirrored wide format
        df = pd.read_csv(matrix,sep="\t", header=None)
        df.columns = ['bin1','bin2','interaction']
        cols=['bin2','bin1','interaction']
        df2=df[cols]
        df2.columns=['bin1','bin2','interaction']
        df_merged=df.append(df2)
        #print(df2)
        df_wide=df_merged.pivot_table(index='bin1', columns='bin2', values='interaction')
        #print(df_wide)
        df_wide_trans=np.transpose(df_wide)
        df_wide=df_wide.fillna(0)
        df_wide.to_csv(r"%s/%s.csv" % (file_path,chrom),sep="\t")
#heatmap=seaborn.heatmap(df_wide)
print("done")



