This was redone during the first revision and is not correct anymore. We still kept the files for the sake of completeness.

These are the scripts used for the analysis in figure 3a. 
To run the scripts you need python 2 and TADbit installed: https://github.com/3DGenomes/TADbit as well as numpy, string and pandas
The analysis consists of three steps:
1. Matrices for each chromosome are converted from hicpro format to square (format_matrix_allchroms.py)
2. TADs are called with tadbit (tadbit.py). This will unfortunately take several hours to run on desktop computer. 
Now that you have the TADs please make a file with all your tads e.g. cat *.tad >> alltads.tad. You will need this for the next step.
3. The center of each microsynteny is calculated. Then the locations are mapped to the TADs called by TADbit

First run the scripts in the folder Tree methods, which creates matrix files for each chromosome. 
Then please run the scripts separately with e.g. python format_matrix_allchroms.py, python2 tadbit.py, python microsyntenies_cut_at_center_1,py
TADbit needs python 2, while the other scripts are using python 3.

To visualize the results, use the R script in R studio. 