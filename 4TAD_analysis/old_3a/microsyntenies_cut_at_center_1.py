#!/usr/bin/python
#get locations of microsyntenic cluster and use the middle of them

#make a dictionary for the start and the stop locations of each synt_id per chromosome
#make a list of all chromosome names
#make a list of all synteny ids per chromosome
#calculate the center of the microsyntenic cluster:
#stop-start=length, so start + middle of length gives the real center


from collections import defaultdict
no_of_genes=defaultdict(list)
list_syntenies=defaultdict(list)
list_of_chroms=[]
center_dict=defaultdict(dict)

print("Please give me the path to your clustering files. They should be in the folder 6Tree_method and the file you want to use (meta.clus or ceph.clus) like this ../6Tree_method/ceph.clus")
file_path= input()
print("How do you want to call your output file?")
outfile=input()
print("Did you make a concatinated TAD file? If not, please merge all files from the tadbit output like this cat *.tad >> alltads.tad")
print("How did you call your concatinated TAD file? Please give me the file with filepath")
tad_file=input()
print("Which resolution did you use? 100000, 40000, 20000? We used 100000 for our figure")
resolution=int(input())
with open("%s" % (file_path)) as microsynteny:
    for line in microsynteny.readlines()[1:]:
        # line=line.replace("..","\t")
        columns = line.rstrip("\n").replace(":", "\t").replace("..", "\t").split("\t")
        # print(columns)
        species = columns[1]
        synt_id = columns[0]
        synt_chrom = columns[7]
        synt_start = int(columns[8])
        synt_stop = int(columns[9])
        genes = columns[11]
        if species == "EUPSC":
            no_of_genes[synt_id] = genes.count("EUPSC")
            synt_center = synt_start + ((synt_stop - synt_start) / 2)
            list_syntenies[synt_chrom].append(synt_id)
            if synt_chrom not in center_dict:
                center_dict[synt_chrom] = {}
            if synt_id not in center_dict[synt_chrom]:
                print(synt_id)
                # start_dict[synt_chrom][synt_id]={}
                center_dict[synt_chrom].update({synt_id: synt_center})
                print(center_dict)
                print("I added synt id %s to %s with center %s; I calculated it from %s and %s" % (
                    synt_id, synt_chrom, synt_center, synt_start, synt_stop))
            else:
                print("This ID is already in the center dict???? ")
                continue

            print("%s\t%s\t%s\t%s\n" % (synt_id, synt_start, synt_stop, synt_center))
    print(center_dict)
    synt_in_tad = 0
    # now we can check where the center of each microsynteny is located within the tads
    with open(tad_file) as tad_file:
        new_file = open(outfile, "w")
        for line in tad_file.readlines():
            columns = line.rstrip("\n").split("\t")
            # print(columns)
            tad_start = (int(columns[0]) - 1) * resolution + 1
            tad_stop = int(columns[1]) * resolution
            tad_intensity = columns[2]
            tad_score = columns[3]
            tad_chrom = columns[4]
            tad_size = ((int(columns[1]) - int(columns[0])) + 1) * resolution
            tad_no = columns[6]
            if tad_chrom in center_dict:
                for synteny in center_dict[tad_chrom]:
                    if (center_dict[tad_chrom][synteny] >= tad_start) and (center_dict[tad_chrom][synteny] <= tad_stop):
                        synt_in_tad = synt_in_tad + 1
                        tad_length = tad_stop - tad_start
                        # print(tad_length)
                        # print(center_dict[tad_chrom][synteny])
                        # print(tad_start)
                        # now that we found the synteny in a tad, we can normalize the locations
                        normalized_center = (
                            (float(center_dict[tad_chrom][synteny]) - float(tad_start)) / float(tad_length))  # elenas idea
                        # print(normalized_center)
                        # print(tad_chrom,synteny,tad_start,tad_stop,normalized_center,no_of_genes[synteny])
                        new_file.write("%s\t%s\t%s\t%s\t%s\t%s\n" % (
                            tad_chrom, synteny, tad_start, tad_stop, normalized_center, no_of_genes[synteny]))
                    else:
                        continue
                        # else:
                        # print("%s is not in range %s "% (center_dict[tad_chrom][synteny], range(tad_start,tad_stop)))
microsynteny.close()
print("I found %s syntenies in TADs yeayy" % (synt_in_tad))
