#!/usr/bin/python2
#import pytadbit
from pytadbit import Chromosome
import string
import os
#import pandas

print("Please give me the direction to the squared matrix files you created with format_matrix_allchroms.py")
file_path=raw_input()
print("Please let me know which resolution you used, e.g. 40000")
res=int(raw_input())
for filename in os.listdir(file_path):
    print(filename)
    if filename.startswith("Lachesis_group") and filename.endswith(".csv"):
        matrix=open("%s/%s" % (file_path,filename))
        chrom=filename.replace(".csv","")
        all = string.maketrans('', '')
        nodigs=all.translate(all, string.digits)
        id=chrom.translate(all, nodigs)
        # initiate a chromosome object that will store all Hi-C data and analysis
        my_chrom = Chromosome(name="%s"%chrom, centromere_search=True,
                      species='Euprymna scolopes', assembly='Lachesis')
        # load Hi-C data
        my_chrom.add_experiment(id, cell_type='wild type', exp_type='Hi-C', identifier=id,
                                project='euprymna_microsynteny',
                                hic_data=matrix, resolution=res)
        my_chrom.find_tad(id, n_cpus=4)
        exp = my_chrom.experiments[id]
        #my_chrom.visualize(id, paint_tads=True) #show heatmap with tads
        print("saving tad file")
        exp.write_tad_borders(density=True,savedata="%s/%s.tad"% (file_path,chrom) )  # write a document with tad boundaries


