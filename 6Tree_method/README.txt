The files in the folder Tree_methods were used to run the neighbour joining method for TAD syntenic consistency profiling (6. in supplementary note).
To run the code you need to have ete3 package installed. 

If you want to test the code open the terminal and move to this folder. Then you can run the file run_tree_method.py by typing python3 run_tree_method.py into your terminal window.
It will ask you for an input file. The input file has to be an iced hi-c matrix as provided by hic-pro and the bed file containing all the bins (also an output file of hic-pro).
We provided three matrices and bed files in the geo project (Resolutions 20000, 40000, 100000). The code will run fastest for 100000 so I would suggest to use this resolution, which still takes a while.
The program will run all steps consecutively and you will get a file with the output that was used for further analysis in R. The columns correspond to the following column names:
"synteny","chromosome","synt_id","farthest_node","distance_root_farthest_node","distance_root_farthest_leaf","n_nodes_extr_tree","n_nodes_cluster","n_genes","bins_total","count","number_of_leafes"

1.  	The first thing the code will do is to divide your contact matrix into chromosomes (extract_chromosomes.py). 
	Please after this step also sort your matrix files by the 3rd column e.g. sort -rnk3 "matrix", otherwise the code will not run!!! You can do: ``` for i in *.matrix; do  	sort -rnk3 $i > $i.sorted;done ```
	to loop through all matrix files in the folder.
2. 	 Second, it will reconstruct which bins have the strongest interaction frequency and create a neighbour joining tree for each chromosome (hic_nj_whole_chroms2.py)
3.  	The last step is to extract the locations of microsyntenic clusters. This code will extract all "sub-trees"/bins that belong to a microsyntenic cluster by ete3s 		get_common_ancestor function.
	For this step you need a file with information of all microsyntenic clusters. We provided two example files (ceph.clus and meta.clus), which includes all cephalopod-	specific and conserved, metazoan. The scripts to create these files are not included in this study, as they were written for Simakov, O. et al. 2013 and Zimmermann, 	Technau & Simakov 2019. Please refer to these papers for the scripts to compute syntenies.
	microsyntenic clusters used for this study.
4.  	If you want to recreate the figure 3b (without the random sampling) you can run the R code in R Studio analyse_synteny_trees_clean.R. 
5. 	To make a heatmap of a subtree run hic_phylogeny_heatmap_100320


	
 Alert: This code only works for Euprymna scolopes files provided in this study and needs some further modifications to be usable for other genomes.