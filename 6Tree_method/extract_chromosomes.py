#!/usr/bin/env python

#sort matrix column 3 by values (sort -rnk3 "matrix")
import os
import re
from collections import defaultdict

list_of_chroms=defaultdict(list)

print("Please specify the exact location of your bed file e.g. /Users/hannah/Documents/Ceph_reg_paper/Sample1_100000_abs.bed")
bed_in=input()
bed=open(bed_in)
print("Please let me know where to save the chromosomal bed and matrix files. Please specify a full file path. Dont use an / at the end please")
out_path=input()
for line in bed.readlines():
    line=line.rstrip("\n")
    columns=line.rstrip("\n").split("\t")
    location=columns[0]
    bin_start=columns[1]
    bin_stop=columns[2]
    bin_id=columns[3]
    if location.startswith("Lachesis_group"):
        list_of_chroms[location].append(line+"\n")
#print(list_of_chroms)

print("I've created a list of all chromosomes, now I will write the separate bed files :)\n They will be named after each chromosome. \n")
for location in list_of_chroms:
    chrom_bed=open("%s/%s.mbed" %(out_path,location),"w")
    for item in list_of_chroms[location]:
        chrom_bed.write(item)
chrom_bed.close()

bin_dict = {}
with open(bed_in) as bed:
    for line in bed.readlines():
        line=line.rstrip("\n")
        columns = line.rstrip("\n").split("\t")
        bin_id=columns[3]
        group_name=columns[0]
        bin_dict[bin_id]=group_name

#print(bin_dict)

chrom_dict=defaultdict(list)

print("Please give me the exact location of your iced matrix file now, including file name like above")
matrix_path=input()
print("Running...")
with open(matrix_path) as matrix:
    for matrix_line in matrix.readlines():
        line=matrix_line.rstrip("\n")
        matrix_columns = matrix_line.rstrip("\n").split("\t")
        matrix_line=matrix_line.rstrip("\n")
        bin1=matrix_columns[0]
        bin2=matrix_columns[1]
        if bin_dict[bin1] == bin_dict[bin2]:
            chromosome=bin_dict[bin1]
            #print(chromosome)
            #print(line)
            chrom_dict[chromosome].append(line +"\n")
        else:
            continue

print("I will now save a matrix file for each chromsome")
for chromosomes in chrom_dict:
    matrix=open("%s/%s.matrix"%(out_path,chromosomes),"w")
    for line in chrom_dict[chromosomes]:
        matrix.write(line)
matrix.close()
print("Thats it, you now have matrix and bed files for each chromosome separately.")

