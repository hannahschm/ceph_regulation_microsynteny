#need to look for "chromosome"
#need to look for start - 100.000 and stop + 100.000
#extract bins, make subset for each microsyntenic cluster
#use ete to prune trees for microsynteny locations
from collections import defaultdict
list_syntenies=defaultdict(list)
start_dict={}
stop_dict={}

#get all ceph-specific microsyntenies and make a dictionary with all synt_ids per chromosome
# as well as the start and stop coordinates of these synt_ids
#add a dictionary that contains the number of genes in each syntenic cluster
#microsynteny=open("/Users/hannah/Documents/hic_phylogeny/meta.clus"# )
print("Please specify the path to your clustering (synteny) files. If you left it where it was, specify the file path to the folder with these scripts.")
file_path_synt=input()
print("We will start with the ceph.clus file")
microsynteny=open("%s/ceph.clus" %(file_path_synt))
#microsynteny=open("/Users/hannah/Documents/synteny_coexpression/meta_random.clus")
#microsynteny=open("/Users/hannah/Documents/synteny_coexpression/ceph_random.clus")


#microsynteny_locations=open("meta_locations.txt","w")
no_of_genes=defaultdict(list)
for line in microsynteny.readlines()[1:]:
    #line=line.replace("..","\t")
    columns=line.rstrip("\n").replace(":","\t").replace("..","\t").split("\t")
    #print(columns)
    species=columns[1]
    synt_id=columns[0]
    synt_chrom=columns[7]
    synt_start=int(columns[8])
    synt_stop=int(columns[9])
    genes=columns[11]
    if species=="EUPSC":
        no_of_genes[synt_id] = genes.count("EUPSC")
        #print(line)
        #print(no_of_genes[synt_id])
        list_syntenies[synt_chrom].append(columns[0])
        #print(species)
        if synt_id not in start_dict:
            start_dict[synt_id]=int(synt_start)
        else:
            continue
        if synt_id not in stop_dict:
            stop_dict[synt_id] = int(synt_stop)
        else:
            continue
    #print(columns[7] + "\t" + columns[8] + "\t" + columns[9] + "\t" + columns[11]+"\n")

#print(no_of_genes)
#open bed file and make a dictionary of all the bin_ids for each chromosome
# as well as the start and stop of these bin_ids
#we need a nested dictionary that tells us the bin_ids for the locations on each chromosome

binstart_id_dict={}
binstop_id_dict={}
list_of_bins=defaultdict(list)
print("Please remind me of the file path to your bed file, including the name")
bed_file_path=input()
bed=open(bed_file_path)
for line in bed.readlines():
    columns = line.rstrip("\n").split("\t")
    chrom=columns[0]
    bin_start = columns[1]
    bin_stop = columns[2]
    bin_id=columns[3]
    if chrom.startswith("Lachesis_group"):
        list_of_bins[chrom].append(bin_id)
        if chrom not in binstart_id_dict:
            binstart_id_dict[chrom] = defaultdict(list)
            binstop_id_dict[chrom] = defaultdict(list)
            binstart_id_dict[chrom][bin_id] = int(bin_start)
            binstop_id_dict[chrom][bin_id] = int(bin_stop)
        else:
            binstart_id_dict[chrom][bin_id] = int(bin_start)
            binstop_id_dict[chrom][bin_id] = int(bin_stop)

bins_in_chrom=defaultdict(list)
for chrom in list_of_bins:
    #print(chrom)
    #print(len(list_of_bins[chrom]))
    bins_in_chrom[chrom]=len(list_of_bins[chrom])

#print(binstart_id_dict)


#loop through the chromosomes and get all the synteny ids for each chromosome. then look for their start and stop and find the closest start and stop to these sequences.
nodes=defaultdict(list)
newstart={}
newstop={}
for chrom in list_syntenies:
    #print(chrom)
    for synt_id in list_syntenies[chrom]:
        #print(bin_id)
        #print(binstart_id_dict[chrom][bin_id])
        for bin_id in list_of_bins[chrom]:
            #print(binstart_id_dict[chrom][bin_id],binstop_id_dict[chrom][bin_id])
            #print(start_dict[synt_id])
            if start_dict[synt_id] > binstart_id_dict[chrom][bin_id] and start_dict[synt_id]<(binstop_id_dict[chrom][bin_id]):
                newstart[synt_id] = bin_id
            if stop_dict[synt_id] > binstart_id_dict[chrom][bin_id] and start_dict[synt_id]<(binstop_id_dict[chrom][bin_id]):
                newstop[synt_id] = bin_id
for synt_id in newstart:
    for bins in range(int(newstart[synt_id]), int(newstop[synt_id])):
        nodes[synt_id].append(str(bins))



#find all subtrees that contain all nodes of a syntenic cluster and find the farthest node distance of these tree as well as the number of bins in the tree
#had to adjust tree.py (ete tool) to continue if it could not find some nodes. need to find out why some nodes are missing, e.g. node 18847 is not in the cluster!
print("Please remind me where you saved your tree files. Can you give me the file path? Your output files will be saved in the same folder")
tree_file_path=input()
print("How do you want to name the output file? This is the file with info for ceph clustering")
new_file=input()
synt_trees=open("%s/%s"%(tree_file_path,new_file),"w")

print(nodes)
from ete3 import Tree
import os


directory=os.fsencode(tree_file_path)
for file in os.listdir(directory):
    filename=os.fsdecode(file)
    if filename.endswith(".tree"):
        treefile = open("%s/%s" % (tree_file_path,filename))
        chrom = filename.replace(".tree", "")
        # print(chrom)
        for line in treefile.readlines():
            line = line.rstrip("\n")
            # print(line)
            tree = Tree(line)
            # print(tree)
            for synt_id in list_syntenies[chrom]:
                # tree_file = open("%s_%s.subtree" % (chrom, synt_id),"w")
                if (nodes[synt_id]) == []:
                    continue
                else:
                    # print(tree)
                    # print((",".join(nodes[synt_id])))
                    # print(tree.get_common_ancestor(["1240","1241","1242","1243"]))
                    # print(chrom)
                    print(synt_id)
                    print(((nodes[synt_id])))
                    subtree = (tree.get_common_ancestor(nodes[synt_id]))
                    # print(subtree)
                    # Calculate farthest node from an internal node
                    farthest, dist = subtree.get_farthest_node()
                    farthest_leaf, dist_leaf = subtree.get_farthest_leaf()
                    print("The farthest leaf from root is", farthest_leaf.name, "with dist=", dist_leaf)
                    print(subtree.get_leaves())
                    no_nodes = (len(subtree))
                    print(nodes[synt_id])
                    orig_no_nodes = (len(nodes[synt_id]))
                    leaf_list = []
                    for leaf in subtree:
                        leaf_list.append(leaf.name)
                    print(orig_no_nodes)
                    print("The farthest node from root is", farthest.name, "with dist=", dist)
                    # print("the number of nodes in this subtree is",node_number)
                    print("ceph\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t1\t%s\n" % (
                        chrom, synt_id, farthest.name, dist, dist_leaf, no_nodes, orig_no_nodes, (no_of_genes[synt_id]),
                        bins_in_chrom[chrom], len(leaf_list)))
                    synt_trees.write("ceph\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t1\t%s\n" % (
                        chrom, synt_id, farthest.name, dist, dist_leaf, no_nodes, orig_no_nodes, (no_of_genes[synt_id]),
                        bins_in_chrom[chrom], len(leaf_list)))

synt_trees.close()
            # tree_file.write(subtree.write())
            # tree_file.close()

            # need to find out why node 18847 is not in the cluster!




print("We will continue with the meta.clus file")
microsynteny=open("%s/meta.clus" %(file_path_synt))

no_of_genes=defaultdict(list)
for line in microsynteny.readlines()[1:]:
    #line=line.replace("..","\t")
    columns=line.rstrip("\n").replace(":","\t").replace("..","\t").split("\t")
    print(columns)
    species=columns[1]
    synt_id=columns[0]
    synt_chrom=columns[7]
    synt_start=int(columns[8])
    synt_stop=int(columns[9])
    genes=columns[11]
    if species=="EUPSC":
        no_of_genes[synt_id] = genes.count("EUPSC")
        #print(line)
        #print(no_of_genes[synt_id])
        list_syntenies[synt_chrom].append(columns[0])
        #print(species)
        if synt_id not in start_dict:
            start_dict[synt_id]=int(synt_start)
        else:
            continue
        if synt_id not in stop_dict:
            stop_dict[synt_id] = int(synt_stop)
        else:
            continue
    #print(columns[7] + "\t" + columns[8] + "\t" + columns[9] + "\t" + columns[11]+"\n")

print(no_of_genes)
#open bed file and make a dictionary of all the bin_ids for each chromosome
# as well as the start and stop of these bin_ids
#we need a nested dictionary that tells us the bin_ids for the locations on each chromosome

#loop through the chromosomes and get all the synteny ids for each chromosome. then look for their start and stop and find the closest start and stop to these sequences.
nodes=defaultdict(list)
newstart={}
newstop={}
for chrom in list_syntenies:
    #print(chrom)
    for synt_id in list_syntenies[chrom]:
        #print(bin_id)
        #print(binstart_id_dict[chrom][bin_id])
        for bin_id in list_of_bins[chrom]:
            #print(binstart_id_dict[chrom][bin_id],binstop_id_dict[chrom][bin_id])
            #print(start_dict[synt_id])
            if start_dict[synt_id] > binstart_id_dict[chrom][bin_id] and start_dict[synt_id]<(binstop_id_dict[chrom][bin_id]):
                newstart[synt_id] = bin_id
            if stop_dict[synt_id] > binstart_id_dict[chrom][bin_id] and start_dict[synt_id]<(binstop_id_dict[chrom][bin_id]):
                newstop[synt_id] = bin_id
for synt_id in newstart:
    for bins in range(int(newstart[synt_id]), int(newstop[synt_id])):
        nodes[synt_id].append(str(bins))



#find all subtrees that contain all nodes of a syntenic cluster and find the farthest node distance of these tree as well as the number of bins in the tree
#had to adjust tree.py (ete tool) to continue if it could not find some nodes. need to find out why some nodes are missing, e.g. node 18847 is not in the cluster!
print("How do you want to name the output file for the metazoan clustering?")
new_file=input()
synt_trees=open("%s/%s"%(tree_file_path,new_file),"w")

print(nodes)
from ete3 import Tree
import os


directory=os.fsencode(tree_file_path)
for file in os.listdir(directory):
    filename=os.fsdecode(file)
    if filename.endswith(".tree"):
        treefile = open("%s/%s" % (tree_file_path, filename))
        chrom = filename.replace(".tree", "")
        # print(chrom)
        for line in treefile.readlines():
            line = line.rstrip("\n")
            # print(line)
            tree = Tree(line)
            # print(tree)
            for synt_id in list_syntenies[chrom]:
                # tree_file = open("%s_%s.subtree" % (chrom, synt_id),"w")
                if (nodes[synt_id]) == []:
                    continue
                else:
                    # print(tree)
                    # print((",".join(nodes[synt_id])))
                    # print(tree.get_common_ancestor(["1240","1241","1242","1243"]))
                    # print(chrom)
                    print(synt_id)
                    print(((nodes[synt_id])))
                    subtree = (tree.get_common_ancestor(nodes[synt_id]))
                    # print(subtree)
                    # Calculate farthest node from an internal node
                    farthest, dist = subtree.get_farthest_node()
                    farthest_leaf, dist_leaf = subtree.get_farthest_leaf()
                    print("The farthest leaf from root is", farthest_leaf.name, "with dist=", dist_leaf)
                    print(subtree.get_leaves())
                    no_nodes = (len(subtree))
                    print(nodes[synt_id])
                    orig_no_nodes = (len(nodes[synt_id]))
                    leaf_list = []
                    for leaf in subtree:
                        leaf_list.append(leaf.name)
                    print(orig_no_nodes)
                    print("The farthest node from root is", farthest.name, "with dist=", dist)
                    # print("the number of nodes in this subtree is",node_number)
                    print("meta\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t1\t%s\n" % (
                        chrom, synt_id, farthest.name, dist, dist_leaf, no_nodes, orig_no_nodes, (no_of_genes[synt_id]),
                        bins_in_chrom[chrom], len(leaf_list)))
                    synt_trees.write("meta\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t1\t%s\n" % (
                        chrom, synt_id, farthest.name, dist, dist_leaf, no_nodes, orig_no_nodes, (no_of_genes[synt_id]),
                        bins_in_chrom[chrom], len(leaf_list)))

synt_trees.close()
            # tree_file.write(subtree.write())
            # tree_file.close()

            # need to find out why node 18847 is not in the cluster!



