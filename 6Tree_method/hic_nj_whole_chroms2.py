#!/usr/bin/env python

#sort matrix column 3 by values (sort -rnk3 *.matrix)

assign = {}  # assigns nodes to bins
tree = {}  # newick tree for every node
cluster = {}  # keeps a list of all bins inside a node
nodeid = 0
height = {}
#lengthfile = open("lengths_group0.csv", "w")

#treefile=open("%s.tree"% chrom, "w")

import os
print("This code will reconstruct interaction trees for each matrix file\n Please sort your chromosomal matrix files with sort -rnk3 *.matrix (in the terminal) otherwise the code will not run.\n Name your new files *.matrix.sorted, e.g. Lachesis_group5__70_contigs__length_158228327.matrix.sorted. If you dont know how, have a look in the README\n")
print("Please specify the exact file path to your matrix files like this: /Users/hannah/Documents/Ceph_reg_paper/matrix_files")
#matrix=open("/Users/hannah/Documents/hic_phylogeny/matrix_lachesis_assembly/Sample1_20000_iced.matrix")
file_path=input()
print("The tree files will be saved to the same folder")
print("Running...")
directory=os.fsencode(file_path)
for file in os.listdir(directory):
    filename=os.fsdecode(file)
    if filename.endswith(".matrix.sorted") and filename.startswith("Lachesis_group"):
        matrix=open("%s/%s" %(file_path,filename))
        chrom=filename.replace(".matrix.sorted","")
        treefile = open("%s/%s.tree" %(file_path,chrom),"w")
        lengthfile = open("%s/%s.length" %(file_path,chrom), "w")
        print(chrom)
        for line in matrix.readlines()[1:]:
            columns = line.rstrip("\n").split("\t")
            bin1 = columns[0]
            bin2 = columns[1]
            similarity = columns[2]
            if bin1 == bin2:
                continue
            if bin1 not in assign:
                nodeid = nodeid + 1
                assign[bin1] = nodeid
                height[nodeid] = 0
                tree[nodeid] = bin1
                cluster[nodeid] = [bin1]
            if bin2 not in assign:
                nodeid = nodeid + 1
                assign[bin2] = nodeid
                height[nodeid] = 0
                tree[nodeid] = bin2
                cluster[nodeid] = [bin2]
            if assign[bin1] == assign[bin2]:
                continue
            nodeid = nodeid + 1
            node1 = assign[bin1]
            node2 = assign[bin2]
            if float(similarity) > 0:
                length = (1 / float(similarity) / 2)
            else:
                length = 0
            height[nodeid] = length
            tree[nodeid] = "(" + tree[node1] + ":" + str(height[nodeid] - height[node1]) + "," + tree[node2] + ":" + str(
                height[nodeid] - height[node2]) + ")"
            cluster[nodeid] = cluster[node1] + cluster[node2]
            #print(tree[node1])
            for bin in cluster[nodeid]:
                assign[bin] = nodeid
                #print(bin)
            #print(tree[nodeid])
            #lengthfile.write("%s\t%s\t%s\t%s\n" %(tree[node1],str(height[nodeid] - height[node1]),tree[node2],str(height[nodeid] - height[node2])))
        lengthfile.close()
        treefile.write(tree[nodeid] + ";")
        treefile.close()

print("Done! I saved your tree-files")

