##this is the latest script, the older scripts have some problems with the matrix formatting
##this is the script used for the edf!


library(ggtree)
library(tidytree)
library(treeio)
library(phylobase)
library(maps)
library(phytools)
library(ape)
library(gplots)
library (tidyverse)
library(dendextend)
library(colormap)

#load the data
#choose a matrix for a chromosome e.g. Lachesis_group1__63_contigs__length_187967259.matrix.sorted
matrix=read.table("")
colnames(matrix)<- c("bin1","bin2","distance")
#choose a bed fileor a chromosome e.g. Lachesis_group1__63_contigs__length_187967259.bed
locations=read.table("")
colnames(locations)<- c("Chromosome","start","stop","bin_id")
#extract locations you would like to plot e.g. of a microsyntenic cluster
#locations of brainy cluster 126741768-127087329
#add plus/minus 1mb and plus/minus 100kb
bin_ids <- locations[locations$start>=126641768 & locations$stop<=127087329, "bin_id"]
#bin_ids <- locations[locations$start>=125741768 & locations$stop<=127187329, "bin_id"]
#bin_ids <- locations[locations$start>=125000000 & locations$stop<=128000000, "bin_id"]

#load the tree (needs to be resaved in figtree, the original file wont work for some reason)
tree=read.tree("")
tree=as.phylo(tree)

#sort tree tips by label names
#first get a vector with unque names
unique_names <- sort(as.numeric(unique(matrix[,1])))

tree_rotated<- rotateConstr(tree,unique_names)
plotTree(tree_rotated)
#tip labels
tips<-c(tree_rotated$tip.label)
#get only the locations of the microsyntenic cluster
#bin_ids_list=c("39355","39356","39357","39358","39359","39360","39361","39362","39363","39364","39365","39366")
#pruned.tree<-drop.tip(tree_rotated,tree_rotated$tip.label[-match(bin_ids_list, tree_rotated$tip.label)])

mrca<-findMRCA(tree_rotated,tips=as.character(bin_ids))

extr_tree<-extract.clade(tree_rotated,mrca)
plot.phylo(extr_tree)

tips<-extr_tree$tip.label

hc <- as.hclust(extr_tree)
dend1 <- as.dendrogram(hc)


#extract the matrix depending on the extracted tips
matrix2 <- subset(matrix, bin1 %in% tips & bin2 %in% tips)

#make similarity matrix
matrix2_similarity <- matrix2
matrix_reordered_similarity <-matrix2_similarity[,c(2,1,3)]
names(matrix_reordered_similarity)<- c("bin1","bin2","distance")
merged_sim <- rbind(matrix2_similarity,matrix_reordered_similarity)
merged_wide_sim<-reshape(merged_sim, idvar = "bin1", timevar = "bin2", direction = "wide")
names(merged_wide_sim) = sub("distance.","",names(merged_wide_sim))
merged_wide_sim2 <- merged_wide_sim[,-1]
rownames(merged_wide_sim2) <- merged_wide_sim[,1]
merged_wide_sim3 <- as.data.frame(merged_wide_sim2)
merged_wide_sim3[is.na(merged_wide_sim3)] <- 0
merged_wide_sim3_sorted <-merged_wide_sim3[order(as.numeric(rownames(merged_wide_sim3) )), order(as.numeric(names(merged_wide_sim3)))]

#add colors to clusters (we only really need this if we want to plot a larger tree)
#par(mar=c(1,1,1,7))
#leafcolor <- colormap(colormap = colormaps$rainbow, nshades = 4, format = "hex", alpha = 1, reverse = FALSE)
#dend1 %>%
  #set("labels_col", value = leafcolor, k=4) %>%
  #set("branches_k_color", value = leafcolor, k = 4) %>%
  #plot(horiz=TRUE, axes=FALSE)
#abline(v = 350, lty = 2)
#dend1 <- set(dend1, "branches_k_color", k = 4, value = leafcolor)
#for tips of dendrogram
#col_labels <- get_leaves_branches_col(dend1)
#col_labels <- col_labels[order(order.dendrogram(dend1))]
#make custom palette in color brewer for contact map
cols<-colorRampPalette(c("white","darkred","red","orange","yellow","white"))(921)

#try ordering by dendrogram
library(data.table)
setcolorder(merged_wide_sim3_sorted, as.character(tips))
#to reorder the rows
#add a column with row names
merged_wide_sim_reordered<-setDT(merged_wide_sim3_sorted, keep.rownames = TRUE)[]
#reorder first column by tips vector
merged_wide_sim_reordered<-as.data.frame(merged_wide_sim_reordered[match(as.character(tips), merged_wide_sim_reordered$rn),])
#convert the first row again to row names (this will only work if you converted to dataframe!!!)
rownames(merged_wide_sim_reordered) <- merged_wide_sim_reordered[,1]
merged_wide_sim_reordered$rn<- NULL
#make heatmap with clusters labelled by color
heatmap.2(as.matrix(merged_wide_sim_reordered), Rowv=dend1,Colv=dend1,trace="none", margins =c(5,7), col=cols, cexRow =0.6,cexCol = 0.8,na.rm = TRUE)
          #RowSideColors = col_labels, # to add nice colored strips        
          #colRow = col_labels, # to add nice colored labels - only for qplots 2.17.0 and higher

#library(ComplexHeatmap)             
#Heatmap(as.matrix(merged_wide_sim3_sorted), name = "test", cluster_columns = dend1)
#now we also want to make a plot of the whole area to be able to compare both

#matrix1 <- matrix[order(as.numeric(matrix$V1)),]
#unique_names <- unique(matrix1[,1])

#similarity matrix for heatmap
matrix2_similarity <- matrix
names(matrix2_similarity)<- c("bin1","bin2","similarity")
matrix_reordered_similarity <-matrix2_similarity[,c(2,1,3)]
names(matrix_reordered_similarity)<- c("bin1","bin2","similarity")
merged_sim <- rbind(matrix2_similarity,matrix_reordered_similarity)
merged_wide_sim<-reshape(merged_sim, idvar = "bin1", timevar = "bin2", direction = "wide")
names(merged_wide_sim) = sub("similarity.","",names(merged_wide_sim))
merged_wide_sim2 <- merged_wide_sim[,-1]
rownames(merged_wide_sim2) <- merged_wide_sim[,1]
merged_wide_sim3 <- as.data.frame(merged_wide_sim2)
merged_wide_sim3[is.na(merged_wide_sim3)] <- 0
merged_wide_sim3_sorted <-merged_wide_sim3[order(as.numeric(rownames(merged_wide_sim3) )), order(as.numeric(names(merged_wide_sim3)))]


#sort tree tips by label names
tree_rotated<- rotateConstr(tree,unique_names)
#tip labels
tips<-c(tree_rotated$tip.label)
#if you are working with a subset: take out only the bins in the matrix that are in the subset of data
merged_wide_sim3_sorted=merged_wide_sim3_sorted[tips,tips] #this command will prune your whole expression matrix to only the rows corresponding to your trees' tips
hc <- as.hclust(tree_rotated)
dend1 <- as.dendrogram(hc)

cols<-colorRampPalette(c("white","darkred","red","orange","yellow","white"))(921)

#get the order of the dendrogram
dend_order<-order.dendrogram(dend1)


setcolorder(merged_wide_sim3_sorted, as.character(tips))

#make heatmap with clusters labelled by color
heatmap.2(as.matrix(merged_wide_sim3_sorted), Rowv=tips,Colv=dend1,trace="none", margins =c(5,7), col=cols, cexRow =0.6,cexCol = 0.8,na.rm = TRUE)


#extract a large region to zoom into tree for the figure
matrix=read.table("") #again insert your file
colnames(matrix)<- c("bin1","bin2","distance")
locations=read.table("") #again insert your file
library (tidyverse)
colnames(locations)<- c("Chromosome","start","stop","bin_id")
#extract locations you would like to plot e.g. of a microsyntenic cluster
#locations of brainy cluster 126741768-127087329
#add plus/minus 100kb
bin_ids <- locations[locations$start>=120000000 & locations$stop<=130000000, "bin_id"]
#load the tree (needs to be resaved in figtree)
tree=read.tree("") #again insert your treef iles
tree=as.phylo(tree)

#sort tree tips by label names
#first get a vector with unque names
unique_names <- sort(as.numeric(unique(matrix[,1])))

tree_rotated<- rotateConstr(tree,unique_names)
plotTree(tree_rotated)
#tip labels
tips<-c(tree_rotated$tip.label)
#get only the locations of the microsyntenic cluster
#bin_ids_list=c("39355","39356","39357","39358","39359","39360","39361","39362","39363","39364","39365","39366")
#pruned.tree<-drop.tip(tree_rotated,tree_rotated$tip.label[-match(bin_ids_list, tree_rotated$tip.label)])

mrca<-findMRCA(tree_rotated,tips=as.character(bin_ids))

extr_tree<-extract.clade(tree_rotated,mrca)
plot.phylo(extr_tree)

tips<-extr_tree$tip.label

hc <- as.hclust(extr_tree)
dend1 <- as.dendrogram(hc)

#extract the matrix depending on the extracted tips
matrix2 <- subset(matrix, bin1 %in% tips & bin2 %in% tips)

#make similarity matrix
matrix2_similarity <- matrix2
matrix_reordered_similarity <-matrix2_similarity[,c(2,1,3)]
names(matrix_reordered_similarity)<- c("bin1","bin2","distance")
merged_sim <- rbind(matrix2_similarity,matrix_reordered_similarity)
merged_wide_sim<-reshape(merged_sim, idvar = "bin1", timevar = "bin2", direction = "wide")
names(merged_wide_sim) = sub("distance.","",names(merged_wide_sim))
merged_wide_sim2 <- merged_wide_sim[,-1]
rownames(merged_wide_sim2) <- merged_wide_sim[,1]
merged_wide_sim3 <- as.data.frame(merged_wide_sim2)
merged_wide_sim3[is.na(merged_wide_sim3)] <- 0
#order the matrix like this, important!! otherwise the plot will be messed up
merged_wide_sim3_sorted <-merged_wide_sim3[order(as.numeric(rownames(merged_wide_sim3) )), order(as.numeric(names(merged_wide_sim3)))]

#add colors to clusters (we only really need this if we want to plot a larger tree)
par(mar=c(1,1,1,7))
leafcolor <- colormap(colormap = colormaps$rainbow, nshades = 60, format = "hex", alpha = 1, reverse = FALSE)
dend1 %>%
  set("labels_col", value = leafcolor, k=60) %>%
  set("branches_k_color", value = leafcolor, k = 60) %>%
  plot(horiz=TRUE, axes=FALSE)
abline(v = 350, lty = 2)
dend1 <- set(dend1, "branches_k_color", k = 60, value = leafcolor)
#for tips of dendrogram
col_labels <- get_leaves_branches_col(dend1)
col_labels <- col_labels[order(order.dendrogram(dend1))]
#make custom palette in color brewer for contact map
cols<-colorRampPalette(c("white","darkred","red","orange","yellow","white"))(921)
#make heatmap with clusters labelled by color
heatmap.2(as.matrix(merged_wide_sim3_sorted), trace="none", margins =c(5,7), col=cols, Colv = dend1, Rowv="NA", cexRow =0.6,cexCol = 0.8,na.rm = TRUE,
          RowSideColors = col_labels, # to add nice colored strips        
          colRow = col_labels, # to add nice colored labels - only for qplots 2.17.0 and higher
) 

setcolorder(merged_wide_sim3_sorted, as.character(tips))

#make heatmap with clusters labelled by color
heatmap.2(as.matrix(merged_wide_sim3_sorted), Rowv=tips,Colv=dend1,trace="none", margins =c(5,7), col=cols, cexRow =0.6,cexCol = 0.8,na.rm = TRUE)

cols<-colorRampPalette(c("white","darkred","red","orange","yellow","white"))(921)
#make heatmap with clusters labelled by color
heatmap.2(as.matrix(merged_wide_sim3_sorted), trace="none", margins =c(5,7), col=cols, dendrogram="col",Colv = dend1, Rowv="NA", cexRow =0.6,cexCol = 0.8,na.rm = TRUE)

