#!/usr/bin/env python

print("If you run this script it will go though each of the steps to analyze the Hi-C matrices.\n Do you want to continue? Y/N")
answer=input()
if answer.lower()=="y":
    print("Lets start with step 1. I will save each chromosome as a separate file. If you want to compare the results to the paper use a bin size of 40000, but 100000 will be faster\n\n")
    exec(open("extract_chromosomes.py").read())
    print("\nDo you want to continue to reconstruct the tree files?Y/N")
    print("Please remember that your matrix files for each chromosome need to be sorted by the third column")
    answer = input()
    if answer.lower() == "y":
        exec(open("hic_nj_whole_chroms2.py").read())
        print("\nDo you want to continue with step 3?Y/N")
        answer = input()
        if answer.lower() == "y":
            exec(open("get_tree_for_all_microsyntenies.py").read())
        else:
            print("ok, its enough for today :)")
    else:
        print("ok, its enough for today :)")
else:
    print("ok, maybe another time :)")
