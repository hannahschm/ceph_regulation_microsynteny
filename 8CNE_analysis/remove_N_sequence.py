#with this script we remove all sequences that contain >25%N, contain <25% N but have fewer than 100 non-N nucleotides and parse the coordinates
#if we want regions that are at last 50bp, we change line 33 char_count - N_count >=100 to char_count - N_count >=50

import re
with open("blastn_esc_obi_noexon_overlapmax3_masked_min100_nb.fa") as fasta:
    outfile=open("blastn_esc_obi_Pasa_norep.bed","w")
    for line in fasta.readlines():
        line.rstrip("\n").split("\t")
        if line.startswith(">"):
            #print(line)
            chrom_match=re.search(r".*?\>(.*)\:.*",line)
            chrom=chrom_match.group(1)
            start_match=re.search(r".*?\:(.*)\-.*",line)
            stop_match = re.search(r".*?\-(.*)", line)
            start=start_match.group(1)
            stop=stop_match.group(1)
            N_count = 0
            char_count = 0
            #print(chrom,start,stop)
        else:
            for character in line:
                #print(character)
                char_count=char_count+1
                if character=="N":
                    N_count = N_count +1
                else:
                    continue
            #print(char_count)
            #print(N_count)
            char_count=char_count-1
            percent_N=N_count/char_count*100
            if percent_N <= 0.25:
                if char_count - N_count >=100:
                    if chrom.startswith("Lachesis_group"):
                        print(chrom,start,stop)
                        print(N_count,char_count)
                        outfile.write("%s\t%s\t%s\n"%(chrom,start,stop))
outfile.close()

