
#we blasted directly against the protein database; now we want to make a bed file with the matching coordinates




#now we want to get a bed file with these locations so we can remove them from our cne bed
#for this we need the table with blast results
#in our blast file we have the euprymna chromosome and locations in column 0, then the gene id; so we can look for each gene id in column 1 and if its there, print column 0

#our genome was split into multiple sections;

with open("blastn_esc_adu_Pasa_norep_sorted_blast_NR.result") as blast_result:
    NR_bed=open("NR_squid.bed","w")
    for line in blast_result.readlines():
        line = line.rstrip("\n")
        columns = line.rstrip("\n").split("\t")
        columns_0=columns[0].replace("-",":").split(":")
        print(columns_0)
        start=int(columns_0[1])+int(columns[6])-1
        stop=int(columns_0[1])+int(columns[7])-1
        chrom=columns_0[0]
        NR_bed.write("%s\t%s\t%s\n"%(chrom,start,stop))
