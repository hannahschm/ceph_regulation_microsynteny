**This is a markdown file explaining all the scripts used for the Euprymna microsynteny project.** 


This bitbucket repository contains all python and R scripts used in the analysis. It will be made available once the paper is accepted. For details on library preparations etc. please consult the appropriate section in the supplementary note. Anything that is not explained here should also be explained in the supplementary note.

All folders contain READMEs that explain what each script does and how to run them, if it is not explained in the general readme. Please look at the READMEs if you want to try running the scripts. 

We did not include the randomized sample files as they are very big, but you can create your own randomized files which will be a little different but comparable.

The different chapters correspond to the supplementary notes.

To run the R scripts, make sure you change the file paths according to your directories.

[TOC]   

**Program versions**   
**Python**: PyCharm Community Edition 2017.2.3,
Python 3.7.3,
Python 2.7.16,   
python3:
ete3 3.1.1
python2:
tadbit 0.4.2

**R**: R studio Version 1.1.414, R Version 3.6.0    
Libraries:    
[ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html) version  3.3.2    
[circlize package](https://github.com/jokergoo/circlize) version 0.4.9.    
gtools version 3.8.2    
GenomicRanges version 1.36.1    
karyoplotR version 1.10.5   
DescTools version 0.99.35   
tidyverse version 1.3.0   
ggtree 1.16.6
tidytree 0.3.3
phylobase 0.8.10
maps 3.3.0
phytools 0.7.47
ape 5.4
gplots 3.0.3
tidyverse 1.3.0
dendextend 1.13.4
colormap 0.1.4
ggpubr 0.3.0
ggridges 0.5.2
argparse 2.0.1
fitdistrplus 1.1.1
cowplot 1.0.0


**Other packages:**  
bedtools   
samtools   
hicexplorer  
hicpro    
lachesis  
deeptools



Color pallet: [Okabe Ito] (https://jfly.uni-koeln.de/color/#pallet)
 

# 2. Genome assembly and analysis
## 2.2 Genome scaffolding

Genome scaffolding based on Hi-C data was done using Lachesis [Burton et al. 2013] (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4117202/). For details on pipeline see [shendurelab/LACHESIS] (https://github.com/shendurelab/LACHESIS). Scaffolds of the published _Euprymna scolopes_ genome were filtered to be at least 50k in size and used as draft de-novo assembly. Bam files aligned to the draft assembly of the two hi-c datasets were used as Hi-C read input (alignment was done as a step in the HicPro pipeline, thus HicPro settings for bowtie were used). RE\_SITE\_SEQ was speficied as AAGCTT. Other settings were: LUSTER\_CONTIGS\_WITH\_CENS = -1,
CLUSTER\_MIN\_RE\_SITES = 25,
CLUSTER\_MAX\_LINK\_DENSITY = 2,
CLUSTER\_NONINFORMATIVE\_RATIO = 3,
CLUSTER\_DRAW\_HEATMAP = 0,
CLUSTER\_DRAW\_DOTPLOT = 1,
ORDER\_MIN\_N\_RES\_IN\_TRUNK = 15,
ORDER\_MIN\_N\_RES\_IN\_SHREDS = 15.

As the exact number of chromosomes is unknown for _Euprymna scolopes_ different numbers for CLUSTER\_N (expected chromosome number) were tried (35, 40, 45, 46, 48, 50, 60), according to the number of other published cephalopod chromosomes. Then, the resulting heatmaps were checked for errors. Setting CLUSTER\_N to 46 resulted in 48 well-defined clusters without obvious errors in the clustering heatmap, while lower or higher settings showed obvious errors in the clustering. 

| Assembly statistics       | |
| ------------- |-------------:| 
| N contigs:    | 3876 |
| Total length:	    | 	5114188061	     | 
| N50: | 3723741      | 
| N clusters (derived): | 48     | 
|N non-singleton clusters: | 48     | 
| N orderings found: | 48     | 
|Number of contigs in clusters:	|3598 (92.83% of all contigs)|
|Length of contigs in clusters:	|5070958786 (99.15% of all sequence length)|
|Number of contigs in orderings:	|2189		(60.84% of all contigs in clusters, 56.48% of all contigs)|
|Length of contigs in orderings:	|4821760194	(95.09% of all length in clusters, 94.28% of all sequence length)|
|Number of contigs in trunks:	|988		(45.13% of contigs in orderings)|
|Length of contigs in trunks:	|1656726814	(34.36% of length in orderings)|
|Fraction of contigs in orderings with high orientation quality:|	2184 (99.77%), with length 4819879074 (99.96%)|
|Fraction of contigs in trunks    with high orientation quality:|	985 (99.7%), with length 1655233218 (99.91%)|


## 2.3 annotation liftover
All scripts and files needed for this part are in the folder [2.3lachesis_liftover](2.3lachesis_liftover).

Genes were lifted over from the published annotation using an in-house script [old_to_new_allchroms_gffformat.py](lachesis_liftover/old_to_new_allchroms_gffformat.py) . The script uses the sizes of old and new scaffolds and the ordering files provided by Lachesis, which contain a list of all scaffolds assembled to a pseudo-chromosome in the assembled order. Between each scaffold 1001 bp are added as Ns by lachesis. 

## 2.4 Mapping in HicPro

Two biological replicates were mapped against the softmasked reference genome without trimming using HicPro. 

Bowtie settings were:
BOWTIE2_GLOBAL_OPTIONS = --very-sensitive -L 30 --score-min L,-0.6,-0.2 --end-to-end --reorder
BOWTIE2_LOCAL_OPTIONS =  --very-sensitive -L 20 --score-min L,-0.6,-0.2 --end-to-end --reorder

Genome Fragment:

Reference genome: euprymna min 50k

Normalization
MAX_ITER = 100
FILTER_LOW_COUNT_PERC = 0.02
FILTER_HIGH_COUNT_PERC = 0
EPS = 0.1


Merged Samples first hicpro:
Valid\_interaction\_pairs	106923369  
Valid\_interaction\_pairs\_FF	26696201  
Valid\_interaction\_pairs\_RR	26682284  
Valid\_interaction\_pairs\_RF	26435507  
Valid\_interaction\_pairs\_FR	27109377  
Dangling\_end\_pairs	11294484  
Religation\_pairs	3489522  
Self\_Cycle\_pairs	704787  
Single-end\_pairs	0  
Dumped\_pairs	168358  

Lachesis hicpro
Valid\_interaction\_pairs	106928662  
Valid\_interaction\_pairs\_FF	26702993  
Valid\_interaction\_pairs\_RR	26687521  
Valid\_interaction\_pairs\_RF	26451618  
Valid\_interaction\_pairs_FR	27086530  
Dangling\_end\_pairs	11294213  
Religation\_pairs	3491065  
Self\_Cycle\_pairs	704973  
Single-end\_pairs	0  
Dumped\_pairs	168420  




# 3. Synteny analysis
## 3.1 Orthology, synteny clustering and randomization of syntenic blocks
**Orthology**


**Microsynteny**    
Scipts are in the folder [3.1Synteny_clustering] (3.1Synteny_clustering).
Microsyntenic blocks were computed as described in [Simakov et al. 2013] (https://www.nature.com/articles/nature11696) and [Zimmermann et al. 2019] (https://www.nature.com/articles/s41559-019-0946-7?proof=trueIn%EF%BB%BF). 
Specifics:    

- minimal length of synteny: 3 genes
- maximum number of intervening genes (Nmax): 5
- maxiumum number of paralogues: 100 
- minimum overlap: 0.3
- minimum species overlap: 0.5

The following 24 species were used for clustering: ![edf2](Images/EDF2_tree_sytenies.pdf) For more information see supplementary note. 
The resulting files were then filtered for metazoan and cephalopod specific syntenies.
**Metazoan** syntenies were defined to be present in at least 7 species out of the 24 initial species. **Cephalopod-specific** synteny was defined to be present in at least 2 cephalopods, but none of the other species. Then clusters present in _Euprymna scolopes_ were used for further analysis, thus all clusters that are present in _Euprymna scolopes_ and 6 other species (275 cluster) were used for the analysis of ancestral, metazoan clusters in _Euprymna_ and all clusters present in _Euprymna scolopes_ and shared with at least one other cephalopod (505 clusters, of which 5 are paralogous clusters) were used for the analysis of novel, cephalopod-specific clusters. Script: [find\_microsynteny\_groups.py](3.1Synteny_clustering/find_microsynteny_groups.py)

To count how many metazoan blocks we find in each species we can simply count the occurance of the species in the first line with ```awk '{count[$2]++} END {for (word in count) print word, count[word]}' meta.clus```

**Block randomization**   
Random microsyntenic blocks were modelled after the distribution of observed microsyntenic blocks as described in Zimmermann et al. 2019] (https://www.nature.com/articles/s41559-019-0946-7?proof=trueIn%EF%BB%BF) using a slightly modified R script[rpz\_pick\_random\_blocks.R](3.1Synteny_clustering/rpz_pick_random_blocks.R). 20 rounds of randomization were sampled, _e.g._ ```
Rscript rpz_pick_random_blocks.R --only-genome=EUPSC -n 20 ceph_random ceph.clus EUPSC.chrom```
where -n is the number of randomizations, ceph\_random is the output folder, ceph.clus is a file with all cephalopod-specific microsyntenies, in the format "synt\_id species ? other\_syntids\_of\_other\_species ? Names\_of\_other\_species ? location gene\_ids" and EUPSC.chrom is a file with the format "Species\_id(EUPSC) Gene\_name Chromosome direction start stop".
 


**EDF2**    
Novel microsyntenies emerging at specific branches of the taxon tree (EDF2) were first filtered with [find\_microsynteny\_groups.py](3.1Synteny_clustering/find_microsynteny_groups.py). Each micro-syntenic cluster had to contain at least 2 species out of the corresponding species list and no species that were not in the list. _E.g._ for lophotrochozoans the pre-filtered microsynteny set contains only clusters with at least 2 lophotrochozoans and no other species. Those filtered files were then used to calculate the number of novel micro-syntenic clusters for each branch: [count\_microsyntenies\_with\_blocks.py](3.1Synteny_clustering/count_microsyntenies_with_blocks.py). 
Paralogous clusters were excluded by using the clustering file with information of all connections between species in each syntenic block and only counting each species once, even if they have more than one connection to a syntenic block).
Specific to calculate novel synteny for each taxon group in EDF2:

|  Taxon       | filtering parameters|
| ------------- |-------------:| 
| Lophotrochozoa:     | At least one species out of each lophotrochozoan group (Mollusca, Annelida and (Platyhelminthes + Rotifera)) or at least one species in Mollusca and one species in Annelida |
| Mollusca:	    | 	At least one species out of all three molluscan groups (Bivalvia, Gastropoda, Cephalopoda)|
| Bivalvia: | Only bivalves    | 
| Gastropoda: | Only gastropods    | 
| Bivalvia and Gastropoda: | At least one bivalve and one gastropod   | Cephalopoda: | At least one squid and one octopus  | 
|Octopoda: |Both octopuses|
|Ecdysozoa: |Nematode and at least one insect|
|Insecta: |At least 2 insects|
|Deuterostomia: |At least one Ambulacraria and one Chordata|
|Chordata: |At least two chordates |
|Ambulacriaria: |Both ambulacrarians |

The tree itself was produced [Mesquite] (https://www.mesquiteproject.org) after latest publications by [Kocot et al. 2020](https://www.nature.com/articles/s41598-019-56728-w) and [Kocot et al. 2016](https://academic.oup.com/sysbio/article/66/2/256/2449704).

## 3.2 Circos plot - Figure 1b  
  
Scripts are in the folder [3.2Circos\_plot](3.2Circos_plot).    
   
Files as input for R were formatted with [prepare\_circos\_3.py] (Circos_plot/prepare_circos_3.py). In short if a syntenic block was present in at least 7 metazoans out of 24 species, it was counted as metazoan. If it was present in at least 2 cephalopods, but no other species, it was counted as cephalopod. Paralogous clusters were excluded, similar to the script used in EDF2. If it was counted in at least 5 mollusks but no other species it was defined as mollusk specific. Results were then filtered to a subset of species to make the plot more easily readable. 
The circos plot was then plotted for 14 of the species in R [Circos_plot.R](Circos_plot/fig1_new_circos_plot200420.R)


## 3.3 Other_scripts - microsyteny distribution and karyoplots - Figure 1c, EDF1, Figure 4
The distribution of length in bp of **Metazoan** microsyntenies and **cephalopod-specific** microsyntenies in _Euprymna scolopes_ (computed as described in 1.1-microsyntenies) from start of first gene to start of last gene in the syntenic block was computed in R [figure1_karyioplot.R](3.3Other_scripts/figure1_karyiplot.R) using ggplot2's geom_density function (figure 1c). The same was done for the density of gene-counts of micro-syntenic blocks in figure 1d. The number of genes was counted in the script described in 4. Neighbour joining method [analyse\_synteny\_trees\_clean.R](6Tree_method/analyse_synteny_trees_clean.R). Intergenic distances were calculated with [gene_distances_synt_type.py] (3.3Other_scripts/gene_distances_synt_type.py).
The script uses a bed file as input that only contains whole genes (start-stop) and only the longest mapped transcripts for gene locations. First, the Euprymna gff was combed for the first and last exon of each gene [get_first_and_last_exon.py](3.3Other_scripts/get_first_and_last_exon.py). Then this file was filtered to only include the longest transcripts [only_longest_transcript.py](3.3Other_scripts/only_longest_transcript.py) and used as an input for  [gene_distances_synt_type.py] (3.3Other_scripts/gene_distances_synt_type.py). The median intergenic distances, median number of genes and median length of clusters were computed with dplyr's summarize(mean) function. The representation of pseudo-chromosome 2 in figure 1d, of _Mizuhopecen yassoensis_ chromosomes in figure 4a and of pseudochromosome 10 in figure 4b was plotted with the KaryoplotR package, as documented in [figure1_karyioplot.R](3.3Other_scripts/figure1_karyiplot.R). 


# 4. Chromatin conformation analysis

## 4.1 TAD prediction
All scripts for this part are in the folder [4TAD_analysis](4TAD_analysis).
TAD prediction was done in [TADbit] (https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005665). 

First all matrices produced in hicpro were converted to square matrices using pythong pivot.table function for each chromosome separatly with [format\_matrix\_allchroms.py](4TAD_analysis/format_matrix_allchroms.py). TAD boundaries were then computed for each chromosome using TADbits default algorithm following the [TADbit tutorial](https://3dgenomes.github.io/TADbit/tutorial.html) (e.g. [tadbit.py](TAD_analysis/tadbit.py)).

This step takes very long!
 
## 4.2 Synteny and TAD composition (Figure 4)

Files needed for this figure are in [4TAD_analysis/figure4] (4TAD_analysis/figure4)
_Mizuhopecten_ and _Euprymna_ pseudochromosomes were plotted with KaryoplotR as described in **1.3**. _Euprymna_ Hi-C maps, RNA-seq and Atac-seq tracks were plotted with [hicexplorer] (https://hicexplorer.readthedocs.io/en/latest/). First, hicpro output files were converted to h5 format with the hicConvertFormat function.    

RNA-seq:
This data is part of an ongoing project and not yet published. Please contact O. Simakov or E. Ritschard for access.

Atac-seq (Genrich called narrow peak files):
97307_Stage20_only.narrowPeak97308_Stage25_only.narrowPeak97309_Stage29_only.narrowPeak103061_stage20_1_only.narrowPeak103062_Stage25_only.narrowPeak103063_Stage29_only.narrowPeak


```
hicConvertFormat --matrices Sample1_40000_iced.matrix --outFileName Sample1_40000_Esc_group1 --inputFormat hicpro --outputFormat h5 --chromosome Lachesis_group1__63_contigs__length_187967259 --bedFileHicpro Sample1_40000_abs.bed --resolutions 40000
```   
Mapped RNA-seq bam files (see **6.Transcriptome sequencing and analysis** for details) and mapped ATAC-seq reads (see **7. Chromatin accessibility assay with ATAC-seq** for details) were converted to bigwick format using samtools and deeptools bamCoverage _e.g._: 

``` 
samtools sort stage20.aligned.out.bam > stage20.sorted.bam 
samtools index stage20.sorted.bam 
bamCoverage -b stage20.sorted.bam -o stage20.bw
```

Genes were plotted as exons converting the _Euprymna_ gff to bed:  

```
awk '{printf "%s\t%s\t%s\t%s\t0\t%s\n", 1,4,5,9, $7}' ../../lachesisliftover/allchromstranscriptids.gff > genes.bed   

bedtools sort -i genesfiltered.bed > genes_sorted.bed
```  

TAD boundaries were plotted as predicted by hicexplorer

```
hicFindTads -m Sample1100000Escgroup1.h5 -–outPrefix TADlachsesisgroup1_100000 –correctForMultipleTesting
```

All of these were added to a ini file and then plotted following hicexplorers tutorial.

## 4.3 Tad averaging (Figure 3a)

During the first review, we found an error in the script to calculate the position of syntenies in Tads. Therefore, this section is crossed out. Further down we describe how we plotted figure 3a (files in [3.3TAD_analysis/new_fig3a](3.3TAD_analysis/new_fig3a) ). The original files can still be found here, but instead one should use the R code in the folder "revision".

~~To understand the distribution of microsyntenies within TADs we found the center of each microsynteny and its location in a TAD, which was then normalized to be able to compare all those syntenies.
The position of microsyntenic clusters in averaged tads were computed with the script [microsyntenies\_cut\_at\_center.py] (3.3TAD_analysis/old_3a/microsyntenies_cut_at_center_1.py) for a resolution of 100000bp/bin. 

The normalized locations were calculated by: 

normalized\_synteny\_location= $\frac{center of synteny - start of TAD}{TAD length}$

The results were further processed in R, first calculating the density distributions for each synteny with ggplot2's geom_density function, then calculating the ratio between the distribution of random vs. observed microsynteny locations:

ratios= $\frac{observed\_synteny}{random\_synteny}$

The results were plotted using ggplot2's geom\_line function [figure3a.R](3.3TAD_analysis/old_3a/figure3a_070520.R).~~

To understand the distribution of microsyntenies within TADs we found the center of each microsynteny and its location in a TAD, which was then normalized to be able to compare all those syntenies. First we used bedops to see where syntenies fall in tads. Ceph.bed and meta.bed are simply bed files with the locations of the syntenies for the analysis.
```
bedmap -u ceph.bed --count --echo --delim '\t' 
``` 

For this, we used [TADs as predicted by TADexplorer] (3.3TAD_analysis/new_fig3a/Tads_100000_sorted_hicexplorer.bed). Then we modified the output files to have all overlaps in each row and remove any ;,| or *. The files can be found here [(3.3TAD_analysis/new_fig3a/ceph_synt_intad2.bed)](3.3TAD_analysis/new_fig3a/ceph_synt_intad2.bed) and [(3.3TAD_analysis/new_fig3a/meta_synt_intad2.bed)](3.3TAD_analysis/new_fig3a/meta_synt_intad2.bed). The first column indicates how many tads overlap the synteny, then they contain synteny and tad information.
For figure 3a, an R sctipt was used that normalizes the Tad middle and calculates its location within the Tad that contains the synteny middle. It was explored to use syntenies that overlap several or only one Tad, but results are similar.

 


## 4.4 Motif finding at TAD boundaries

To identify possible motifs enriched at TAD boundaries, the locations of tadbit predicted TAD boundaries were extracted and converted to a bed file, converting the startbin to start with tad_start = (int(columns[0]) - 1) * 100000 +1 and the stop bin with
tad_stop = int(columns[1]) * 100000. The regions were then checked for enriched motifs using homer    
```findMotifsGenome.pl all_tads_boundaries.bed Euprymna_genome.fasta -size 200```



# 6. Neighbour-jonining method
All scripts for this part can be found in the folder [6Tree_method](6Tree_method/).
Only chromosomal scale scaffolds were considered for this analysis. Unassigned and unordered scaffolds were filtered out prior to analysis. 

1. The Hi-C iced matrix and bed files (hic-pro output) were split into separate matrix and bed files for each chromosome:
[extract\_chromosomes.py](6Tree_method/extract_chromosomes.py)
The files then need to be sorted _e.g._ like this in the termianl:```sort -rnk3 *.matrix```

2. Compute "interaction trees" for each chromosome. This means we take the intensity of the interaction between bins to cluster them together; two bins with the highest interaction become "sister groups" to each other, then they cluster with the bins with the next highest interaction until all bins are integrated in the tree. 
Script: [hic\_phylogeny\_whole\_chroms2.py](6Tree_method/hic_phylogeny_whole_chroms2.py)
The script saves every chromosome as a newick tree file and it saves the length of the branches. 

3. Compute all the information we can get from the tree files. To understand how well a region is defined by its interactions we extract the last common ancestor of that region (the bins in that region) from the whole tree making up the chromosome. We then construct a table with information about the syntenic clusters and their tree structure _e.g._ synteny-type (cephalopod-specific, ancestral (metazoan) synteny), chromosome, synt-id, farthest node, distance root farthest node,  distance root to farthest leaf, number of nodes in the extr tree, number of nodes cluster, number of genes, bins total, count, number of leafes in the extracted tree. For that we extract the tree containing all the bins of a synteny-id by its last common ancestor. If the synteny location is well defined by its interaction, the ratio between the nodes of the tree and the bins making up the synteny id should be close to 1 [get\_tree\_for\_all\_microsyntenies.py](6Tree_method/get_tree_for_all_microsyntenies.py)

4. Continue with R for statistical testing and analysis: The best way to figure out if the syntenies are well-defined by their interaction properties is to calculate the ratio between the number of bins in the extracted tree for a syntenic cluster (by last common ancestor) and the number of initial bins in the syntenic cluster. Ratios higher than 1 were excluded.
For boxplots and wilcox test use [analyse\_synteny\_trees\_clean.R](6Tree_method/analyse_synteny_trees_clean.R) (this script needs to be changed if other resolutions than 40000bp are tried)      Different parameters were tested: 
     - using the whole matrix
     - filtering for clusters with at least 4 genes
     - using only clusters within a certain size range: minbin 15 and max bin 50 for 20000bp resololution, minbin 7 and maxbin 25 for 40000bp resultuion and minbin 3 and maxbin 10 for 100000bp resolution - this seems to be the best solution as it reduces the bias that the random syntenies are generally larger than the observed syntenies

5. EDF 7: to plot heatmaps with trees use script: [hic\_phylogeny\heatmap\_100320.R](6Tree_method/hic_phylogeny_heatmap_100320.R) I tried extracting the brainy cluster both with 40000 and 100000 resolution; while 100000 seems to work great, 40000 did not work well at all. Made a summary plot of the method for the paper, showing the whole chromosome, a zoom in and the microsyntenic cluster (hic_phylogeny_edf.pdf). 

# 7 Co-expression correlation (figure 3c)

To test whether genes in microsyntenic clusters are more likely to be co-expressed than genes that randomly sit in close proximity in the genome we sampled the genome 20x by the distribution of cephalopod-specific and metazoan syntenies (number of genes)(see https://doi.org/10.1038/s41559-019-0946-7), extracting clusters of genes in close proximity that are not syntenic. We then calculated the co-expression coefficient of genes in observed and random microsyntenies followin Zimmermann et al. 2019. For the first revision, we noticed a bug in the fisher correction and updated the script accordingly. Hemocytes were excluded from the analysis and genes with zero expression were excluded as well as microsyntenic clusters with less than three genes after this filtering. Significance of results were tested with unpaired two-sided wilcox test in ggstatsplot. Script and files can be found here:
[7Co-expression analysis](7Co-expression analysis)

##clustering of expression (figure 3d)
The R script and files for this figure can be found in [7Co-expression analysis](7Co-expression analysis/clustering)

# 8 CNE analysis 

Python scripts can be found in the folder 9CNE_analysis


- length min 100bp or 50bp
- can be up to 800kb from target genes in human
- similarity threshold 70-98%
- sizes 30 or 50 - remove anything that overlaps by 1 or more bp with an exon




###Step 1: Megablast alignments between squid species or octopus and squid
Whole genome alignments between _E. scolopes_ and _O. bimaculoides_ as well as between _E. scolopes_ and _D. pealeii._

Steps are done on the example of the _O.bimaculoides_ and _E. scolopes_ alignments.

The scripts to split genome alignments are property by O. Simakov. Please contact him for further information or to get the scripts.

perl splitFaMegablastVISTA095.pl Octopus_hardmasked.fa 100 Euprymna_hardmasked.fa 100000 10 1000


Merge resuting files and get table:
perl /proj/Simakov/scripts/WGA/vistaFilter.pl sort > blastn_esc_obi.res.filt


###Step 2: create bed files from blast results
Make a bedfile of headers:  

```awk '{printf "%s\t%s\t%s\n",$2,$7,$8}' blastn_esc_obi.res.filt > blastn_esc_obi.bed```

If start is bigger than stop, we reverse 
```awk '$2 > $3 { temp = $3; $3 = $2; $2 = temp } 1' OFS='\t' blastn_esc_obi.bed> blastn_esc_obi_ordered.bed```

###Step 3: filtering exons, repeats and matches on NR database
Next we remove anything that overlaps with an exon:
We convert the gff file to bed 
```awk '{printf "%s\t%s\t%s\n",$1,$4,$5}' PASA.gff3  > esc_pasa.bed```

Remove the chromosome name:
```awk '{gsub("__.*", "", $1); print}'  esc_pasa.bed> esc_PASA_renamed.bed```

```cat esc_PASA_renamed.bed | tr -s '[:blank:]' '\t' > esc_PASA_renamed.bed1.bed ```

```sort -nk1 esc_PASA_renamed.bed1.bed  > esc_PASA_renamed_sorted.bed```

Now we need to remove all alignments that overlap with exons or other possible coding regions:

First, we sort the bed file with aligned regions:
```sort-bed  blastn_esc_obi_ordered.bed > blastn_esc_obi_sorted.bed```

Then we need to look at regions that might be hitting the same target several times;

module load bedops

```
bedmap --count --echo --fraction-both 0.5 --delim '\t' blastn_esc_obi_sorted.bed| awk '$1<4' | cut -f2- | sort-bed - | uniq > blastn_esc_obi_overlapmax3.bed

bedops --merge  blastn_esc_obi_overlapmax3.bed > blastn_esc_obi_overlapmax3_merged.bed
```
Now we can subtract these regions using bedtools.

```
bedtools subtract esc_PASA_renamed_sorted.bed -A  > blastn_esc_obi_noexon_overlapmax3_merged.bed```


Because there are probably still some repetitive regions, we filter these out too. 

``` 
bedtools getfasta -fi Euprymna_hardmasked.fa -bed blastn_esc_obi_noexon_overlapmax3_merged.bed > blastn_esc_obi_noexon_overlapmax3_merged.fa ```


We need to mask these regions:
mask simple repeats - use dust to get rid of simple repeats


``` 
module unload perl
module load meme
dust blastn_esc_obi_noexon_overlapmax3_merged.fa 10 > blastn_esc_obi_noexon_overlapmax3_masked.fa 
```


Then we need to get rid of the line breaks

```awk '/^>/ { print (NR==1 ? "" : RS) $0; next } { printf "%s", $0 } END { printf RS }'  blastn_esc_obi_noexon_overlapmax3_masked.fa > blastn_esc_obi_noexon_overlapmax3_masked_min100_nb.fa ```



Now we need to filter for alignments of length min 100bp or 50bp

The python script [9CNE\_analysis/remove\_N\_sequence.py](9CNE\_analysis/remove\_N\_sequence.py) that removes all regions that have more than 25% N or are smaller than 100bp or 50bp (please modify script in this case). 

The results of this script will in the following commands be called blastn_esc_obi08_min100filtered.bed

We still had some rRNA sequences left, therefore we blasted the resulting files against the NCBI NR database:

```bedtools getfasta -fi ../Lachesis_assembly.fasta.masked -bed  blastn_esc_obi_min100filtered.bed> blastn_esc_adu_min100filtered.fa```

This script is property to O. Simakov, for further information please contact him.
```
perl /proj/Simakov/scripts/splitFaBlastGeneral.pl blastx blastn_esc_obi_min100filtered.fa 100 /localmirror/monthly/blast/ncbi/nr 1 1
```
```
cat *.blast > blastn_esc_obi_min100filtered.result
```

Then we use a custom python script to convert to bed file and filter out these regions:
[9CNE\_analysis/remove\_coding\_regions\_NR.py](9CNE\_analysis/remove\_coding\_regions\_NR.py)


remove any overlapping regions

```bedops --merge NR_ceph.bed> NR_cephmerged.bed```

sort the new file
 ```awk '$2 > $3 { temp = $3; $3 = $2; $2 = temp } 1' OFS='\t' NR_cephmerged.bed> NR_cephmerged_sorted.bed```
 
```sortBed -i NR_cephmerged_sorted.bed > NR_cephmerged_sorted2.bed```

now we can remove these regions

```bedtools subtract -A -a blastn_esc_obi_min100filtered.bed -b NR_cephmerged.bed_sorted.bed > blastn_esc_obi_min100filtered_noNR.bed```







# 9. Chromatin accessibility assay with ATAC-seq

##9.2 ATAC-seq - Mapping, quality control and peak calling
Quality of reads was assessed with fastqc (https://bioinformatics.babraham.ac.uk/projects/fastqc/). Trimming of reads was done with ```bbduk (ktrim=r qtrim=15 k=21 mink=8 hdist=0)``` from the bbtools package (https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/ version 38.56). Reads were mapped to the E. scolopes chromosomal assembly using bowtie238 (Version 2.3.5.1) (--very-sensitive -k 10 -p 8). Samtools21 (Version 1.10) fixmate was run on aligned bam files, then the command ```samtools view -f $(samtools flags PROPER_PAIR,READ1 | cut -f 1) *.fixed.bam | awk '{print $9}'> *insert_sizes.txt``` to extract insert sizes. Insert sizes were plotted in R.    
To get a better understanding of the location of peaks, we annotated the genome using PASA39 (Version 2.3.3). The same transcripts that were used for the initial E. scolopes annotation were used and it was run on the new chromosomal assembly. PASA was run with ```Launch_PASA_pipeline.pl -C -c PASAconfig.txt -R -g Euprymna_chormosomal_assembly.fasta -t Eup.final.fna --ALIGNER S gmap --CPU 8```.   
Afterwards, the output was compared to the published E. scolopes annotation and annotated with the same gene names. Then only the longest transcript for each gene was retained if there were still several isoforms left. The PASA annotation retained 16097 annotated genes (compared to 24378 in our original GFF file). 
Peaks were called on aligned bam files with ```Genrich (-j -y -r -v)``` (https://github.com/jsh58/Genrich). Reproducible peaks were called from replicates using IDR (Irreproducible discovery rate, https://github.com/nboley/idr), resulting in 50-61.8% of peaks passing the IDR cutoff of 0.05. Peaks were annotated to genomic regions with ChIPseeker40 using the new PASA annotation. Promoter regions were annotated as +10 kb and -10 kb from the transcription start site. If peaks were annotated to a gene belonging to a specific microsyntenic cluster, they were annotated to ceph/meta or non syntenic. Overlaps of peaks were plotted with limma41, peaks overlapping between replicates were identified with soGGi’s42 runConsensusRegions function following the ATACseq in Bioconductor tutorial (https://rockefelleruniversity.github.io). All peaks overlapping with microsyntenic regions were extracted using the findOverlaps function of the GenomicRanges package.
Input files and R script can be found in:
[9atac-seq/Figure3e](9atac-seq/Figure3e)
##9.3 Motif finding
To find any enriched motifs in regions of ATAC-seq peaks, locations of microsyntenic clusters falling into specific expression modules were extracted. Only peaks that were present in both replicates of ATAC-seq samples were used. Those peaks were extracted using 

```
bedtools intersect -wa stage*_1.genrich_peaks -b  stage*_2.genrich_peaks | sort | uniq > intersect_stage*.bed
```
Where stage*\_\* represent the respective replicates for each stage. 
Then, using bedtools intersect, regions of all microsyntenies falling into an expression module were compared to atac-seq peaks and intersecting regions were extracted _e.g._:  

```
bedtools intersect -a expression_module_*.bed -b intersect_stage*.bed> module_intersect_atac_stage*.bed
```
where expression_module_*.bed represents a bed file with the locations for all microsyntenic clusters falling into one of the expression modules.

Then motifs were identified for all atac peaks overlapping a specific cluster using homer findMotifsGenome.pl using the default size parameter (=200). Summarized results for p-values <=0.001 are found in supplementary table 1. 

#Hic files resolution 150000 (figure 2 and 4 3D organisation)
The folder hic_140000 [hic_150000](hic_150000) contains input files (binsize 140000) for 3D reconstructions of microsynteny in figure 2 and 4.

# Supplementary figures
The folder [Suppl_figs](Suppl_figs/) contains scripts and datafiles for each supplementary figure. If files are already in the bitbucket in another folder, it is indicated here.

## 1: Supplementary\_figure\_1

Supplementary figure 1a is automatically created by the lachesis program when re-assembling the data (see also supplementary methods of our paper). b) and c) can be created using files and the R script (figure1_karyoplot.R) in 3.3Other_scripts with the R package karyoplotR. 

## 2: Supplementary\_figure\_2
Files and scripts to get numbers for each branch can be found in the folder 3.1Synteny_clustering.
Barplots can be run with the R script in 3.3O0ther_scripts (figure1_karyoplot.R).

## 3: Supplementary\_figure\_3

The folder [Suppl\_figs/Suppl\_fig\_3](Suppl_figs/Suppl_fig_3) contains scripts and input files used to make the go-term analysis and figure. The script was run with weighted01 for the figures in the paper. 

## 4: Supplementary\_figure\_4
Input sequences and alignment files for supplementary figure 3 are found in [Suppl\_figs/Suppl\_fig\_3](Suppl_figs/Suppl_fig_3). Sequences for 3c were provided by [Heger et al. 2020] (https://elifesciences.org/articles/45530).

## 5: Supplementary\_figure\_5


##6 : Supplementary\_figure\_6

##7 : Supplementary\_figure\_7
You also need to run the script for figure 3 (esc heatmap) to get the names of the syntenies in each euprymna cluster.
Fisher correction still needs to be updated and changed in the figure!

For suppementary figure 7a, the script [coexpression\_correlation\_obi.R] (Suppl_figs/Suppl_fig_7/coexpression_correlation_obi.R) and the corresponding input files ("obi\_meta\_synteny\_expression.txt","obi\_ceph\_synteny\_expression.txt","obi\_meta\_random\_synteny\_expression.txt","obi\_ceph\_random\_synteny\_expression.txt") were used.
For suppementary figure 7b and c, the script [heatmap\_bubbleplot\_obi.R] (Suppl_figs/Suppl_fig_7/heatmap_bubbleplot_obi.R) and the corresponding input files ("obi\_meta\_synteny\_expression-with-esc-ids.txt","obi\_ceph\_synteny\_expression-with-esc-ids.txt") were used. Additionally, the script for figure 3d needs to be run.

##8: Supplementary\_figure\_8
Using the clustering of figure 3d and the expression values of each synteny, one can calculate the relative expression, contribution of expression of each gene and standard deviation in each of the expression clusters. Additonally, the tmm values for Euprymna genes are provided here to calculate the contribution of expression for each gene (esc_tmm_expr.txt in folded [Suppl_figs/Suppl_fig_8/](Suppl_figs/Suppl_fig_8/) .

## 9&10: Supplementary\_figure\_9 and 10
A similar figure can be plotted using karyoplotR, gene densities (see karyoplotR folder in [3.3.Other\_scripts] (3.3Other_scripts)) and the output files of the CNE analysis provided in [8CNE\_analysis/result\_files] (8CNE_analysis/result_files) . An R script is provided in the folder for Suppl fig. 9.

