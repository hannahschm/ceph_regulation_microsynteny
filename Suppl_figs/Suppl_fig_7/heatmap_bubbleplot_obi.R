#heatmap for octopus (figure 3) with euprymna clustering
library(tidyverse)
library(purrr)
library(viridis)
library(circlize)
library(dplyr)
library(dendextend)
library(data.table)
library(ComplexHeatmap)
library(RColorBrewer)
#function to take out rows with no expression and calculate mean per syntenic block
synteny_mean_expression<-function(table,columns){
  expression=read.table(table,header=FALSE,sep=",")
  names=c("Gene","Anc","Ova","Psg","St15","Retina","Sub","Sucker","Testes","Viscera","Supra","OL","Skin","Synt_id","Synt_id_esc")
  colnames(expression)<-names
  expression<-expression[,c(columns)]
  expression <- expression[order(expression$Synt_id_esc),] 
  expression2 <- expression[,-1]
  rownames(expression2) <- expression[,1]
  dfs <- split(expression2, expression2$Synt_id_esc)
  dfs<-lapply(dfs,function(x){x[1:(length(x)-1)]})
  dfs1<-lapply(dfs,function(x){
    x[rowSums(x)>0,]
  })
  #remove all matrices that have less than three rows
  ind <- which(sapply(dfs1,function(x){
    nrow(x)>2
  }))
  print(ind)
  filtered_tdfs <- dfs1[ind]
  print(filtered_tdfs)
  lapply(filtered_tdfs,function(x){colMeans(x,na.rm=FALSE)
  })
}

#tissues=c(1,2,3,4,6,7,8,9,10,11,12,13,15)
#we should order the tissues in a way that makes senes, so all genes with brain together, then eyes, skin etc
#Anc,supra,sup,OL,retina,skin,suckers,psg,viscer,test...
tissues=c(1,2,11,7,12,6,13,8,4,10,9,3,15)
#would it make more sense to take out tissues?


#we need a file that has the euprymna synt_ids instead of the octopus syntids, this we made with 
meta<-synteny_mean_expression("obi_meta_synteny_expression-with-esc-ids.txt",tissues)
ceph<-synteny_mean_expression("obi_ceph_synteny_expression-with-esc-ids.txt",tissues)

#calculate means
metameans<-do.call("rbind",meta)
#calculate means
cephmeans<-do.call("rbind",ceph)

#add synteny column
cephmeans<-as.data.frame(cephmeans)
cephmeans$synt_type<-c("ceph")
metameans<-as.data.frame(metameans)
metameans$synt_type<-c("meta")

#merge the matrices
means<-rbind(cephmeans,metameans)
synt_type_annot<-means$synt_type
synt_type_annot<-as.data.frame(synt_type_annot)
rownames(synt_type_annot) <- rownames(means)

#for scaling rows in heatmap
cal_z_score <- function(x){
  (x - mean(x)) / sd(x)
}

#normalize the matrix for plotting
norm <- t(apply(as.matrix(means[,c(1,2,3,4,5,6,7,8,9,10,11)]), 1, cal_z_score))
#remove all rows with standard dev 0
norm[is.na(norm)] <- 0
norm[is.nan(norm)] <- 0

#lets first do a clustering just for octopus

all_norm_hclus_obi <- hclust(dist(norm), method = "complete")
#lets have a look

as.dendrogram(all_norm_hclus_obi) %>%
  plot(horiz = TRUE)

my_gene_col <- cutree(tree = as.dendrogram(all_norm_hclus_obi), k = 8)

#transform to two column df (wide to long)
genes_long<-gather(data.frame(as.list(my_gene_col)), synt_id, cluster)
#remove x in front of columns
genes_long$synt_id <- substring(genes_long$synt_id, 2)

#look for gene annotations per cluster in synteny and the ratio of meta and ceph synteny compared to all meta/ceph genes
#1st the ratio
#add the synteny column
synt_type1<-setDT(synt_type_annot, keep.rownames = TRUE)[]
genes_long$synt_type <- synt_type_annot$synt_type_annot[match(genes_long$synt_id, synt_type_annot$rn)]
library(dplyr)
count(genes_long,synt_type)


#we should then sort the synt type to match the norm df
#first we would need to load the df
library(viridis)
library(circlize)

heatmap_colors <- circlize::colorRamp2(breaks = c(-2,-1,0,1, 2),
                                       colors = c("royalblue2","lightskyblue2","white", "yellow", "red"),
                                       transparency = .2)

#also get colors for the clusters so we can label them the same way both on the heatmap and scatterplot
test<-data.frame(synt_type_annot$synt_type)
colnames(test)<-c("synt_type")
annot<-HeatmapAnnotation(df=test, col=list(synt_type=c("ceph"="#0072B2","meta"="#E69F00")),which="row")
cluster_colors<-data.frame(genes_long$cluster)
colnames(cluster_colors)<-c("clusters")
annot2<-HeatmapAnnotation(df=cluster_colors,col=list(clusters=c("1"="#1B9E77","2"="#D95F02","3"="#7570B3","4"="#E7298A","5"="#66A61E","6"="#E6AB02","7"="#A6761D","8"="#666666","9"="#77DAD5","10"="#0181BB","11"="#fc766a","12"="#DEDE0E","13"="#7f0c50")),which="row")
Heatmap(norm, cluster_rows = as.dendrogram(all_norm_hclus_obi),cluster_columns=TRUE,col=heatmap_colors,row_split=8,border = TRUE,left_annotation=annot,right_annot=annot2)

#now we can start working on the bubble plot
#save the obi clusters
genes_long_obi<-genes_long
#get a table with the number of syntenic clusters that fall into each expression module
genes_long_obi$count<-1
cluster_counts_obi<-genes_long_obi%>%group_by(cluster)%>%summarize(total_per_cluster=sum(count))
############careful here you need to run another script before continuing################
#then we run the euprymna script until we have the euprymna heatmap (Figure3.R) to get the euprymna genes
#genes long is now overwritten with the euprymna list
genes_long_esc <- genes_long
genes_long_esc$count<-1
cluster_counts_esc<-genes_long_esc%>%group_by(cluster)%>%summarize(total_per_cluster=sum(count))

#now we have the list for both and we can merge them
genes_long_merged<-merge(genes_long_obi,genes_long_esc,by="synt_id")
#only keep the important columns
genes_long_merged<-genes_long_merged[,c(1,2,5,3)]
#rename columns
colnames(genes_long_merged)<-c("synt_id","cluster_obi","cluster_esc","synt_type")

genes_long_merged$cluster_obi<-as.factor(genes_long_merged$cluster_obi)
genes_long_merged$cluster_esc<-as.factor(genes_long_merged$cluster_esc)
#now we want to have all pairwise combinations and count how many synt_ids we have in there
combinations<-genes_long_merged%>%group_by(cluster_obi, cluster_esc)%>%summarise(length(unique(synt_id)))%>%mutate(combination = paste(cluster_obi, cluster_esc, sep=", "))   

colnames(combinations)<-c("cluster_obi","cluster_esc","sum","cluster_combination")

ggplot(combinations, aes(x=cluster_obi, y=cluster_esc, size = sum, color=sum)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1.4, 19)) +
  theme(legend.position="bottom")+
  scale_color_gradient(low="#009E73", high="#F0E442")+
  theme_light()


#lets try to also do what oleg suggested
#get total number of clusters in each module

#first merge so that we have comparable numbers, because we will only keep everything that is in both dfs
genes_long_merged<-merge(genes_long_obi,genes_long_esc,by="synt_id")
genes_long_merged<-genes_long_merged[,c(1,2,5,3)]
colnames(genes_long_merged)<-c("synt_id","cluster_obi","cluster_esc","synt_type")


genes_long_merged<-genes_long_merged%>%group_by(cluster_obi)%>%mutate(cluster_n_obi=n())
genes_long_merged<-genes_long_merged%>%group_by(cluster_esc)%>%mutate(cluster_n_esc=n())
#now we want the synt_type sum devided by min(cluster_obi, cluster_esc)
combinations<-genes_long_merged%>%group_by(cluster_obi, cluster_esc)%>%summarise(length(unique(synt_id)))%>%mutate(combination = paste(cluster_obi, cluster_esc, sep=", "))
colnames(combinations)<-c("cluster_obi","cluster_esc","sum","cluster_combination")
#now we need to add the cluster ns to combinations
test<-unique(genes_long_merged[,c("cluster_obi","cluster_n_obi")])
test2<-unique(genes_long_merged[,c("cluster_esc","cluster_n_esc")])
combinations<-merge(combinations,test,by="cluster_obi")
combinations<-merge(combinations,test2,by="cluster_esc")

test<-unique(genes_long_merged[,c("cluster_obi","cluster_n_obi")])
test2<-unique(genes_long_merged[,c("cluster_esc","cluster_n_esc")])


combinations$min_score<-combinations$sum/pmin(combinations$cluster_n_obi,combinations$cluster_n_esc)
combinations$cluster_obi<-as.factor(genes_long_merged$cluster_obi)
combinations$cluster_esc<-as.factor(genes_long_merged$cluster_esc)

#####this is the figure used in supplentary figure 7 #######
ggplot(combinations, aes(x=cluster_obi, y=cluster_esc, size = min_score, color=min_score)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1.4, 19)) +
  theme(legend.position="bottom")+
  scale_color_gradient(low="#009E73", high="#F0E442")+
  theme_light()
###########################################################
ggplot(combinations, aes(x=cluster_obi, y=cluster_esc, size = sum, color=sum)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1.4, 19)) +
  theme(legend.position="bottom")+
  scale_color_gradient(low="#009E73", high="#F0E442")+
  theme_light()



#lets try to divide the numbers for each group by the their clusters


library(plyr)

#make the same pie chart for obi as we have in esc
counts<-ddply(genes_long_obi, .(genes_long_obi$cluster, genes_long_obi$synt_type), nrow)
detach(package:plyr)

colnames(counts)<-c("cluster","synt_type","counted")
counts2 <- counts %>% group_by(cluster) %>% mutate(per = as.numeric(counted)/sum(as.numeric(counted)))
#make barplots per cluster
#order the clusters the way they are displayed in the heatmap
counts2$cluster <- factor(counts2$cluster, levels=c("3","5","4","2","8","6","1","7"))
ggplot(counts2, aes(x=cluster, y=per, fill=synt_type)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values=c("#0072B2","#E69F00"),aesthetics = "fill")+ 
  coord_flip()+
  scale_x_discrete(limits = rev(levels(counts2$cluster)))+ #reverse the x axis  
  theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
                     panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"))





